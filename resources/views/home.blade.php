@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col mt-2">
            <div class="card">
                <div class="card-header">Bienvenue</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="alert alert-success" role="alert">Vous êtes connecté</div>

                    <div class="text-center">
                        <a href="{{ route("/") }}"> Page d'accueil </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
