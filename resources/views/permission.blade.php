@extends('layouts.app')

@section('content')
    <div class="container mt-4 text-center">
        <ul class="list-group mt-3">
            <li class="list-group-item list-group-item text-center bg-info ">
                <div>SUPER ADMINISTRATEURS DU SITE</div>
                @foreach ($super as $user)  
                    <li class="list-group-item list-group-item">
                        <div>
                            <img alt="img" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;">
                            {{ $user->firstname }} {{ $user->lastname }}
                        </div>
                        @if(Auth::check() && Auth::user()->hasRole('superadmin'))
                            <a href="{{ route('deleteRole', $user->id) }}" class="btn btn-danger mt-3" type="submit"><img src="{{ asset("storage/img/delete.png") }}" alt="logo" width="20px"></a>
                        @endif
                    </li>
                @endforeach
            </li>
        </ul>

        <ul class="list-group">
            <li class="list-group-item list-group-item text-center bg-info">
                <div>ADMINISTRATEURS DU SITE</div>
            </li>
            @foreach ($admins as $user)  
                <li class="list-group-item list-group-item">
                    <div>
                        <img alt="{{ $user->lastname }} {{ $user->firstname }}" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;">
                        {{ $user->firstname }} {{ $user->lastname }}
                    </div>
                    @if(Auth::check() && Auth::user()->hasRole('superadmin'))
                        <a href="{{ route('deleteRole', $user->id) }}" class="btn btn-danger mt-3" type="submit"><img src="{{ asset("storage/img/delete.png") }}" alt="logo" width="20px"></a>
                    @endif
                </li>
            @endforeach
        </ul>

        <ul class="list-group">
            <li class="list-group-item list-group-item text-center bg-info">
                <div>MEMBRES DE L'ASSOCIATION</div>
            </li>
            @foreach ($members as $user)  
                <li class="list-group-item list-group-item">
                    <div>
                        <img alt="img" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;">
                        {{ $user->firstname }} {{ $user->lastname }}
                    </div>
                    @if(Auth::check() && Auth::user()->hasRole('superadmin'))
                        <a href="{{ route('deleteRole', $user->id) }}" class="btn btn-danger mt-3" type="submit"><img src="{{ asset("storage/img/delete.png") }}" alt="logo" width="20px"></a>
                    @endif
                </li>
            @endforeach
        </ul>

        <form class="d-flex justify-content-between mt-3" action="{{ route('permissionSearch') }}" method="POST">
            @csrf
            <input class="form-control mr-1" type="search" placeholder="Ajouter un admin" aria-label="Search" name="searchBar">
            <button class="btn btn-info mr-1" type="submit" style="width:25%">Rechercher</button>
        </form>

        @isset($results)
            <ul class="list-group mt-1">
                @foreach ($results as $res)
                    <li class="list-group-item list-group-item-success d-flex justify-content-between align-items-center">
                        <img alt="img" src="{{ asset($res->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;">
                        {{ $res->firstname }} {{ $res->lastname }}
                        <div>
                            @if(Auth::check() && Auth::user()->hasRole('superadmin'))
                                <a href="{{ route('addSuperAdmin', $res->id) }}" class="btn btn-danger" type="submit">Ajouter en super admin</a>
                            @endif

                            <a href="{{ route('addAdmin', $res->id) }}" class="btn btn-warning" type="submit">Ajouter en admin</a>
                            <a href="{{ route('addMember', $res->id) }}" class="btn btn-success" type="submit">Ajouter en membre</a>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endisset

        <a href="{{ route('deleteRole', Auth::user()->id) }}" class="btn btn-danger mt-3" type="submit">SE SUPPRIMER DES ADMINS @if(Auth::check() && Auth::user()->hasRole('superadmin')) ET SUPER-ADMINS @endif</a>
    </div>
@endsection