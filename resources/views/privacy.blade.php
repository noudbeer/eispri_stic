@extends('layouts.app')

@section('content')
    <div class="container mt-4 mb-4">
        <h2 class="mt-4 text-center page-title">Politique de confidentialité<h2>

        <div class="dropdown-divider py-2 mt-4" style="color:#900a59"></div>

        <p class="mb-4">
            <h3 style="color: #900a59">Traitement des données personnelles</h3>
            <div class="mb-3">
                <p>Aucune donnée personnelle n’est collectée à l’insu des utilisateur de eispri-stic.fr</p>
                <p><b>Données collectées :</b></p>
                <p>
                    Le site internet recueille les coordonnées des utilisateurs à des fins de gestion et de suivi de la demande de ceux-ci. Les données saisies par l'utilisateur dans les formulaires sont stockées dans une base de donnée MySQL du site internet.
                </p>
            </div>
        </p>

        <p class="mb-4">
            <h3 style="color: #900a59">Cookies</h3>
            <div class="mb-3">
                <p>
                    <ul>
                        <li>XSRF-TOKEN : Permet une protection sur les formulaires.</li>
                        <li>eispri_stic_session :  permet une identification de session active.</li>
                    </ul>
                </p>
            </div>
        </p>

        <p class="mb-4">
            <h3 style="color: #900a59">Clause de non-responsabilité</h3>
            <div class="mb-3">
                <p>
                    Les informations proposées sur eispri-stic.fr sont de nature indicatives, non contractuelles et ne visent en aucun cas la situation particulière d’une personne physique ou morale.
                </p>

                <p>EISPRI STIC se réserve le droit de modifier le contenu de ce site à tout moment sans préavis.</p>

                <p>
                    Toutes les erreurs signalées seront immédiatement corrigées. EISPRI STIC n’apporte aucune garantie sur l’utilisation des informations fournies sur son site internet. L'association ne sera pas responsable des conséquences pouvant résulter de l'utilisation des informations présentes sur le site.
                </p>
                <p>Des liens présents sur ce site web redirigent les utilisateurs vers d’autres sites dont le contenu ne peut engager la responsabilité d'EISPRI STIC.</p>
            </div>
        </p>
    </div>
@endsection