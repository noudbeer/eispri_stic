@extends('layouts.app')

@section('content')
    <div class="container md-4" style="background:white">
        <h2 class="mt-4 text-center page-title">SELECTION D'UN ÉVÉNEMENT</h2>

        <div class="mt-4" style="background: white">
            <div class="row">
                @foreach($events as $event)
                    <a href="{{ Route('teams.showTeams', $event->id) }}" class="col-md-4">
                        <div class="card mb-4">
                            <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                            <div class="card-body">
                                <div class="card-title">
                                    <div>
                                        {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                    </div>
                                    <h5 class="card-title">{{ $event->name }}</h5>
                                </div>
                                <p class="card-text">
                                    {!! nl2br($event->description_excerpt) !!}
                                </p>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection