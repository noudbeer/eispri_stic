@extends('layouts.app')

@section('content')
    <div class="container mt-4 mb-4">
        <h2 class="mt-4 text-center page-title">Liste des partenaires<h2>

        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nom de l'entreprise</th>
                <th scope="col">Adresse email du contact</th>
                <th scope="col">Année du partenariat</th>
                <th scope="col">Montant de la donnation</th>
                <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($partners as $partner)
                    <tr>
                        <th scope="row">{{ $partner->id }}</th>
                        <td>{{ $partner->name }}</td>
                        <td>{{ $partner->email }}</td>
                        @if($partner->lastDonation() != null)
                            <td>{{ $partner->lastDonation()->year }}</td>
                            <td>{{ $partner->lastDonation()->amount }}</td>
                        @else
                            <td></td>
                            <td></td>
                        @endif    
                        <td class="d-flex justify-content-around text-center">
                            <a href="#" class="btn btn-warning m-1">MAJ</a>
                            <a href="#" class="btn btn-danger m-1">Historique</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection