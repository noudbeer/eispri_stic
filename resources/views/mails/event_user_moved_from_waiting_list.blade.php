<p>Bonjour {{ $user->firstname }},</p>
<p>Nous sommes heureux de vous annoncer qu'une place de l'évènement « <a href="{{ route('event.show', $event->id) }}"><strong>{{ $event->name }}</strong></a> » s'est libérée, et que vous y avez été par conséquent inscrit !<br>
<p></p>

<p>Si vous avez changé d'avis, vous pouvez vous désinscrire en vous rendant sur <a href="{{ route('event.show', $event->id) }}">la page de l'évènement</a>.</p>
<p></p>

<p><em>Merci de ne pas répondre à cet e-mail. Pour nous contacter, veuillez vous rendre sur <a href="{{ route('contact') }}">la page de contact</a>.</em></p>
