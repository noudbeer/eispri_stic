<p>Bonjour {{ $user->firstname }},</p>
<p>Nous confirmons votre inscription à l'évènement « <a href="{{ route('event.show', $event->id) }}"><strong>{{ $event->name }}</strong></a> »
@if (isset($team) && $team !== null)
    dans l'équipe « <a href="{{ route('event.show_team', $team->id) }}"><strong>{{ $team->name }}</strong></a> »
@endif
.</p>
<p></p>

<p>Si vous avez changé d'avis, ou que vous n'avez pas sollicité cette action, vous pouvez vous désinscrire en vous rendant sur <a href="{{ route('event.show', $event->id) }}">la page de l'évènement</a>.</p>
<p></p>

<p><em>Merci de ne pas répondre à cet e-mail. Pour nous contacter, veuillez vous rendre sur <a href="{{ route('contact') }}">la page de contact</a>.</em></p>
