<p>Bonjour {{ $user->firstname }},</p>
<p>En raison d'un trop grand nombre de participants, vous avez été inscrit à la liste d'attente de l'évènement « <a href="{{ route('event.show', $event->id) }}"><strong>{{ $event->name }}</strong></a> ».<br>
Deux possibilités s'offrent à vous :</p>
<ul>
    <li>Si un participant se désiste, vous serez automatiquement inscrit à sa place.</li>
    <li>Si l'évènement suscite une trop forte demande, nous étudieront la situation et feront en sorte d'ouvrir des places supplémentaires, ou de créer un évènement annexe.</li>
</ul>
<p>Nous vous recontacterons dès que la situation évoluera.</p>
<p></p>

<p>Si vous avez changé d'avis, vous pouvez vous désinscrire en vous rendant sur <a href="{{ route('event.show', $event->id) }}">la page de l'évènement</a>.</p>
<p></p>

<p><em>Merci de ne pas répondre à cet e-mail. Pour nous contacter, veuillez vous rendre sur <a href="{{ route('contact') }}">la page de contact</a>.</em></p>
