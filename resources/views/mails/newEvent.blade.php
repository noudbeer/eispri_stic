<div class="container">
    <div class="card mb-4">
        <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
        <div class="card-body">
            <div class="card-title">
            <div>
                {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
            </div>
                <h5 class="card-title">{{ $event->name }}</h5>
            </div>
            <p class="card-text">
                {!! nl2br($event->description_excerpt) !!}
            </p>

            <div id="registration" class="mt-3 d-flex justify-content-between">
                <div class="text-right">
                    <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                </div>
            </div>
        </div>
    </div>
</div>

