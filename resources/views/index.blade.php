@extends('layouts.app')
@section('content')
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            @if($event !== null) 
                <li data-target="#carousel" data-slide-to="1"></li> 
            @endif
        </ol>

        <div class="carousel-inner">
            @if($event !== null)
                <div class="carousel-item active">
                        <img src="{{ asset("storage/img/events/$event->id") }}" class="d-block w-100 img-slider" alt="{{ $event->name }}">
                        <div class="carousel-caption d-none d-md-block">
                            <h3 class="title-carousel">PROCHAIN ÉVÉNEMENT</h3>
                            <h4 class="title-carousel">{{ $event->name }}</h4>
                            <a href="{{ route('event.show', $event->id) }}">En savoir plus</a>
                        </div>
                </div>
            @endif
            <div class="carousel-item @if($event == null) active @endif">
                <img src="{{asset("storage/img/caroussel_default.png")}}" class="d-block w-100 img-slider" alt="club.info.jpg">
                    <div class="carousel-caption d-none d-md-block">
                        <h3 class="title-carousel">À partir de la L3</h3>
                        <a href="#join" {{--target="_blank"--}}>En savoir plus</a>
                    </div>
            </div>
        </div>

        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="container md-4" style="background:white" role="main">
        <h1 class="mt-5" id="EISPRI_STIC">EISPRI STIC (BDE informatique)</h1>
        <mark class="strong">Présentation</mark> <br>
        <p>
            <div class="strong">EISPRI STIC</div> ou <div class="strong">E</div>tudiant en <div class="strong">I</div>nformatique de <div class="strong">S</div>avoie : <div class="strong">P</div>romouvoir, <div class="strong">R</div>approcher et <div class="strong">I</div>ntégrer les <div class="strong">I</div>nformaticiens des <div class="strong">S</div>ciences et <div class="strong">T</div>echnologies de l'<div class="strong">I</div>nformation et des <div class="strong">C</div>ommunications<br/>
            est une association étudiante (BDE) qui a pour but de réunir les étudiants en informatique ainsi que leurs professeurs afin de mieux se connaître et pouvoir créer une bonne ambiance dans la filière.<br>
            Soirées et évènements informatiques sont au programme afin d'intégrer et rapprocher les étudiants ainsi que promouvoir la filière.<br>
        </p>

        <div class="strong">Aujourd'hui</div><br>
        <p>
            L'association fait chaque année 4 événements intitulés « L'intégration », le « Forum des métiers de l'informatique » appelé aussi « RAID informatique », « La nuit de l'info » et la remise des diplômes.<br>
            <ul>
                <li><div class="strong">L'intégration</div> est un événement festif qui a pour but d'intégrer les premières années ainsi que les arrivants des autres cursus.<br></li>
                <li><div class="strong">Le forum des métiers de l'informatique</div> a pour but d'aider les étudiants de Licence 3 à Master 2 à rechercher de stages et/ou des alternances.<br></li>
                <li><div class="strong">La nuit de l'info</div> est une compétition informatique nationale en équipe qui donne aux étudiants des problèmatiques à résoudre en une nuit.</li>
                <li><div class="strong">La remise des diplômes</div> permet de remettre et de fêter la réussite des nouveaux diplômés.</li>
            </ul>
            D'autres <a href="{{ route("events.index") }}">événements</a> seront organisés pendant l'année.
        </p>

        <mark class="strong" id="office">Bureau de l'association</mark><br>
        <p>
            <ul>
                <li>Président : Zohir Belhaouzia</li>
                <li>Vice-président : Loris Lacerenza</li>
                <li>Trésorière : Juliette Blanc</li>
                <li>Secrétaire : Théo Jubard</li>
                <li>Responsable technique : Léo Laffont</li>
            </ul> 
        </p>

        @if($nb_members > 0)
            <mark class="strong" id="members">Membres</mark><br>
            <p>
                <ul>
                    @foreach ($members as $member)
                        <li>{{ $member->firstname }} {{ $member->lastname }}</li>
                    @endforeach
                </ul>
            </p>
        @endif

        <mark class="strong" id="projets">Projet
        	@if (count($events) > 1)
               	s
            @endif
         en cours</mark><br>
        <p>
        	@if (count($events) == 0)
               	<p>Il n'y a aucun projet informatique en cours en ce moment.</p>
            @else
	            <ul>
	                @foreach ($events as $event)
	                    <li>{{ $event->name }}</li>
	                @endforeach
	            </ul>
            @endif
        </p>

        {{--
        <mark class="strong" id="parteners">Partenaires 2019/2020</mark><br>
        <div class="row justify-content-center">
            @foreach($partners as $partner)
                <div class="card col-md-4 mb-4 " style="border:none;">
                    <img src="{{ asset("storage/img/partners/$partner->logo") }}" class="card-img-top" style="height: 100px;" alt="logo {{ $partner->name }}">
                </div>
            @endforeach
        </div>
        --}}
        
        @if(Auth::check() && Auth::user()->hasRole('user'))
            <mark id="join" class="strong">Nous rejoindre</mark><br>
                <p style="margin-top: 5px;">Si vous êtes intéréssé pour nous rejoindre, vous pouvez rejoindre l'association en payant <strong>5€ de cotisations TTC</strong> afin de participer à l'organisation des évènements. Vous pourrez également participer aux votes pour les grandes décisions de l'association. Nous sommes à la recherche de nouveaux membres peut importe votre année universitaire.</p>
                <div id="smart-button-container" style="align-items: center;">
                  <div style="text-align: center; width: 250px; margin-top: 10px; margin-left: auto; margin-right: auto;">
                    <div id="paypal-button-container"></div>
                  </div>
                </div>
    	    <script src="https://www.paypal.com/sdk/js?client-id=AW5pXBFsHtrG9AvzWYhLXS6bd6zZBbZK1mDn_NhRUajZaizCbFt-V0ApVo8wcDVdLTkBa6yZGkjbb8Q4&enable-funding=venmo&currency=EUR" data-sdk-integration-source="button-factory"></script>
            <!-- <script src="https://www.paypal.com/sdk/js?client-id=AW5pXBFsHtrG9AvzWYhLXS6bd6zZBbZK1mDn_NhRUajZaizCbFt-V0ApVo8wcDVdLTkBa6yZGkjbb8Q4"> // Required. Replace YOUR_CLIENT_ID with your sandbox client ID. -->
            </script>
    	    <script>
    	        function initPayPalButton() {
    	           paypal.Buttons({
        	        style: {
        	          shape: 'pill',
        	          color: 'blue',
        	          layout: 'vertical',
        	          label: 'pay',
        	          
        	        },

    	        createOrder: function(data, actions) {
    	          return actions.order.create({
    	            purchase_units: [{"description":"Devenir membre de l'association.","amount":{"currency_code":"EUR","value":5}}]
    	          });
    	        },

    	        onApprove: function(data, actions) {
    	          return actions.order.capture().then(function(orderData) {
    	            
    	            // Full available details
    	            console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

    	            // Show a success message within this page, e.g.
    	            const element = document.getElementById('paypal-button-container');
    	            element.innerHTML = '';
    	            element.innerHTML = '<h3 style="color:#38c172;font-weight: bold;">Merci d\'avoir rejoint l\'association !</h3>';

    	            // Or go to another URL:  actions.redirect('thank_you.html');
    	            
    	          });
    	        },

    	        onError: function(err) {
    	          console.log(err);
    	        }
    	       }).render('#paypal-button-container');
    	       }
    	       initPayPalButton();
            </script>
        @endif
        @if(!(Auth::check()))
            <mark id="join" class="strong">Nous rejoindre</mark><br>
                <p style="margin-top: 5px;">Si vous êtes intéréssé pour nous rejoindre, vous pouvez rejoindre l'association en payant <strong>5€ de cotisations TTC</strong> afin de participer à l'organisation des évènements. Vous pourrez également participer aux votes pour les grandes décisions de l'association. Nous sommes à la recherche de nouveaux membres peut importe votre année universitaire.</p>
                <div id="smart-button-container" style="align-items: center;">
                  <div style="text-align: center; width: 250px; margin-top: 10px; margin-left: auto; margin-right: auto;">
                    <div id="paypal-button-container"></div>
                  </div>
                </div>
            <p><i>Vous devez vous <a href="./login"><b>connecter</b></a> pour pouvoir payer via Paypal ou carte bancaire.</i></p>
        @endif
        @if(Auth::check() && !(Auth::user()->hasRole('user')))
            <mark id="join" class="strong">Nous rejoindre</mark><br>
            <p><i>Vous avez déjà rejoint l'association.</i></p>
        @endif
    </div>
@endsection
