@extends('layouts.app')

@section('content')
    <div class="container md-4" style="background:white">
        <h2 class="mt-4 text-center page-title">Créer une facture</h2>

        <form class="text-center" enctype="multipart/form-data" method="POST" action="{{ route('manageInvoice') }}">
            @csrf
            <div class="form-group">
                <label for="name">Nom de l'entreprise</label>
                <input type="name" class="form-control text-center @error('name') is-invalid @enderror" id="name" name="name" required>

                @error('name')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="address" name="label-address">Adresse postale de l'entreprise</label>
                <textarea class="form-control @error('address') is-invalid @enderror" rows="3" placeholder="N° &#10;Rue &#10;Code postale + nom de la ville" id="address" name="address"></textarea>

                @error('address')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="email" name="label-address">Adresse email de l'entreprise</label>
                <input class="form-control @error('email') is-invalid @enderror" id="email" name="email">

                @error('address')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>


            <div class="form-group">
                <label for="subject">Sujet de la facture</label>
                <input type="subject" class="form-control text-center @error('subject') is-invalid @enderror" id="subject" name="subject" required>

                @error('subject')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

           <div class="form-group">
                <label for="price">Montant</label>
                <input type="number" step="0.01" class="form-control text-center @error('price') is-invalid @enderror" id="price" name="price">

                @error('price')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="deadline">A payer avant le</label>
                <div>
                    <input type="date" id="deadline" name="deadline" class="@error('deadline') is-invalid @enderror">
                </div>

                @error('deadline')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="mb-2">
                <button class="btn btn-outline-dark" type="submit" name="preview" value="true">Prévisualiser la facture</button>
            </div>

            <div class="mb-4">
                <button class="btn btn-outline-dark" type="submit" name="download">Enregistrer la facture</button>
                <button class="btn btn-success" type="submit" name="send">Envoyer la facture</button>
            </div>
        </form>
@endsection
