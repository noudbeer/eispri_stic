@extends('layouts.app')

@section('content')
    <div class="container mt-4 mb-4">
        <h2 class="mt-4 text-center page-title">Mentions légales<h2>

        <div class="dropdown-divider py-2 mt-4" style="color:#900a59"></div>

        <p class="mb-4">
            <h3 style="color: #900a59">Identification de l'éditeur</h3>
            <p class="mb-3">
                EISPRI_STIC, pour "Etudiant en Informatique de Savoie : Promouvoir, Rapprocher et Intégrer les Informaticiens de Science et Technologie de l'Information et des Télécommunication", association régie par la loi 1901.
            </p>
            <p class="mb-3">
                Adresse du siège social :<br>
                    <p class="ml-4">
                        GUIDE<br>
                        Batiment Tarentaise - Bureau 010<br>
                        Campus Scientique<br>
                        Université de Savoie<br>
                        73376 BOURGET-DU-LAC CEDEX
                    </p>
                </p>
            <p class="mb-3">
                Adresse courriel : asso.eispri.stic@gmail.com<br>
                Numéro secrétaire : 0666532283<br>
                Numéro vice-secrétaire : 0787667430<br>
            </p>
            <p class="mb-3">N° RNA : W732002743</p>
        </p>

        <p class="mb-4">
            <h3 style="color: #900a59">Directeur de publication</h3>
            <p class="mb-3">
                « Au sens de l’article 93-2 de la loi n°82-652 du 9 juillet 1982 » : Monsieur Zohir BELHAOUZIA, président
            </p>
        </p>

        <p class="mb-4">
            <h3 style="color: #900a59">Prestataires d’hébergement</h3>
            <p class="mb-3">
                <b>Pour eispri-stic.fr :</b>
                <p class="ml-4">
                    Infomaniak<br>
                    Rue Eugène-Marziano 25, 1227 Genève, Suisse<br>
                    Tel. : +41228203544
                </p>
            </p>
        </p>
    </div>
@endsection