@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
        <div class="card-body">
            <h4 class="card-title">{{ $event->name }}</h4>
            <p>{{ $event->beginning->format('d/m/Y H\hi') }} - {{ $event->end->format('d/m/Y H\hi') }}<br>{{ $event->location }}</p>

            <p class="card-text">{!! nl2br($event->description) !!}</p>
            <hr>

            @if (!$event->registrationEnded())

                <h4 class="text-center">
                    {{-- @if ($event->room !== null)
                        {{ $event->room }} places -
                    @endif --}}

                    @if ($event->price > 0)
                        Tarif : {{ $event->price }} €
                    @else
                        Inscription gratuite
                    @endif
                </h4>

                <h5 class="text-center mb-4">Fermeture des inscriptions {{ $event->registrationEnd()->format('\l\e d/m/Y \à H\hi') }}</h5>
                {{-- <h5 class="text-center">Pour toute modification après votre inscription, veuillez <a href="{{ route('contact') }}">nous contacter directement</a>.</h5> --}}

            @else
                <h5 class="text-center">Inscriptions terminées</h5>
            @endif

            <div id="registration" class="mt-3 d-flex justify-content-between">
                <div class="text-left d-flex">
                    @if ($event->team())
                        <a href="{{ route('teams.showTeams', $event->id) }}" class="btn btn-outline-primary pull-left mr-1">Voir les équipes</a>
                    @else
                        <a href="{{ route('event.showParticipants', $event->id) }}" class="btn btn-outline-primary pull-left mr-1">Voir les inscrits</a>
                    @endif

                    @if(!$event->registrationEnded())
                        @if (!Auth::check())

                            <a href="{{ route('login') }}" class="btn btn-outline-success pull-left">S'inscrire</a>

                        @elseif($event->participates(Auth::user()))

                            <div class="d-flex align-content-center flex-wrap">
                                @if ($event->team())
                                    <a href="{{ $event->teamRoute(Auth::user()) }}" class="btn btn-outline-success pull-left mr-2">Mon équipe</a>
                                @else
                                    <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                @endif

                                <div class="align-self-center">Vous êtes inscrit à cet évènement</div>
                            </div>

                        @elseif($event->inWaitingList(Auth::user()))

                            <div class="d-flex align-content-center flex-wrap">
                                <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                <div class="align-self-center">Vous êtes en liste d'attente pour cet évènement</div>
                            </div>

                        @else
                            <a href="{{ $event->registrationRoute() }}" class="btn btn-outline-success pull-left">S'inscrire</a>
                        @endif
                    @endif
                </div>
                <div class="text-right">
                    <a href="{{ route('/') }}" class="btn btn-outline-primary pull-left">Partager</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
