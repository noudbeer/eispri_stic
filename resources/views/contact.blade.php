@extends('layouts.app')

@section('content')
    <div class="container md-4" style="background:white">
            <h2 class="mt-4 text-center page-title">NOUS CONTACTER</h2>

            <p>Pour nous contacter, vous pouvez :</p>

            <h5>Rejoindre notre serveur Discord</h5>
            <p><a href="https://discord.gg/gRSS9hH">Cliquez ici !</a></p>

            {{-- <h5>Pour toute demande liée à la Nuit de l'Info 2019</h3>
            <p><a href="mailto:nuit-info@eispri-stic.fr">nuit-info@eispri-stic.fr</a></p> --}}

            <h5>Nous envoyer un e-mail</h5>
            <p>Pour tout problème ou toute demande liée au site : <a href="mailto:webmaster@eispri-stic.fr">webmaster@eispri-stic.fr</a><br>
            Pour toute autre demande liée à l'association : <a href="mailto:asso.eispri.stic@gmail.com">asso.eispri.stic@gmail.com</a></p>

            {{--
            <form>
                @guest
                <div class="form-group">
                    <label for="InputEmail1">Votre adresse email :</label>
                    <input type="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Entre ton adresse mail" required>
                    <small id="emailHelp" class="form-text text-muted">Votre adresse email ne sera pas divulgué</small>
                </div>
                @endguest

                <div class="form-group">
                    <label for="inputState">Type de demande :</label>
                    <select id="inputState" class="form-control" required>
                        <option selected>Entre ton type de demande</option>
                        <option>Partenariat</option>
                        <option>Don</option>
                        <option>Demande presse</option>
                        <option>Autre demande</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="InputObject">Objet du message :</label>
                    <input type="object" class="form-control" id="Object" aria-describedby="" placeholder="Entre ton objet" required>
                </div>

                <div class="form-group">
                    <label for="InputMessage">Votre message :</label>
                    <textarea class="form-control" id="Message" placeholder="Entre ton message" style ="height:25vh" required></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Envoyer</button>
            </form>
            --}}

            <br>
    </div>
@endsection
