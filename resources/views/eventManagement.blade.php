@extends('layouts.app')

@section('content')
    <div class="container md-4" style="background:white">
        <h2 class="mt-4 text-center page-title">GESTION DES ÉVÉNEMENTS</h2>

        <div>
            <nav>
                <div class="nav nav-tabs text-center" id="tab" role="tablist">
                    <a class="nav-item nav-link active col" id="nav-new-events-tab" data-toggle="tab" href="#new-events-pane" role="new" aria-controls="new-envents-pane" aria-selected="true">Les prochains événements</a>
                    <a class="nav-item nav-link col" id="nav-obsolete-events-tab" data-toggle="tab" href="#obsolete-events-pane" role="obsolete" aria-controls="obsolete-events-pane" aria-selected="false">Les événements passés</a>
                <div>
            </nav>
            <div class="tab-content mt-4" id="TabContent">
                    <div class="tab-pane fade show active" id="new-events-pane" role="tabpanel" aria-labelledby="new-events-pane">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="{{ route('addEventPage') }}" role="button">
                                    <div class="card mb-4">
                                        <img class="card-img-top p-5" src="{{ asset('storage/img/add.png') }}"  />
                                        <div class="card-body">
                                            <h5 class="card-title text-center">Ajouter un événement</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @foreach ($nextEvents as $event)
                                <div class="col-md-4">
                                    <div class="card mb-4">
                                        <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                                        <div class="card-body">
                                            <div class="card-title">
                                            <div>
                                                {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                            </div>
                                                <h5 class="card-title">{{ $event->name }}</h5>
                                            </div>
                                            <p class="card-text">
                                                {!! nl2br($event->description_excerpt) !!}
                                            </p>
                                            <div class="d-flex justify-content-between">
                                                <a href="{{ route('deleteEvent', $event->id) }}" class="btn btn-outline-danger pull-right">Supprimer</a>
                                                <a href="{{ route('editEventPage', $event->id) }}" class="btn btn-outline-warning pull-right">Modifier</a>
                                                <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="tab-pane fade" id="obsolete-events-pane" role="tabpanel" aria-labelledby="obsolete-events-pane">
                        <div class="row">
                            @foreach ($pastEvent as $event)
                                <div class="col-md-4">
                                    <div class="card mb-4">
                                        <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->is }}">
                                        <div class="card-body">
                                            <div class="card-title">
                                            <div>
                                                {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                            </div>
                                                <h5 class="card-title">{{ $event->name }}</h5>
                                            </div>
                                            <p class="card-text">
                                                {!! nl2br($event->description_excerpt) !!}
                                            </p>
                                            <div class="d-flex justify-content-between">
                                                <a href="{{ route('deleteEvent', $event->id) }}" class="btn btn-outline-danger pull-right">Supprimer</a>
                                                <a href=" {{ route('editEventPage', $event->id) }}" class="btn btn-outline-warning pull-right">Modifier</a>
                                                <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
