@extends('layouts.app')

@section('content')
    <div class="container md-4" style="background:white">
        <h2 class="mt-4 text-center page-title">NOS PROCHAINS ÉVÉNEMENTS</h2>

        <div class="mt-4" style="background: white">
            <div class="row">
                @foreach ($nextEvents as $event)
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                            <div class="card-body">
                                <div class="card-title">
                                <div>
                                    {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                </div>
                                    <h5 class="card-title">
                                        {{ $event->name }} <br>
                                        <span class="text-info">{{ $event->nbParticipants() }} 
                                        @if ( $event->nbParticipants() )
                                            participants
                                        @else
                                            participant
                                        @endif
                                        </span>
                                    </h5>
                                </div>
                                <p class="card-text">
                                    {!! nl2br($event->description_excerpt) !!}
                                    @if($event->registrationEnded())
                                        <br>
                                        <span class="text-danger">Inscriptions terminées.</span>
                                    @endif
                                </p>

                                <div id="registration" class="mt-3 d-flex justify-content-between">
                                    <div class="text-left d-flex">
                                        @if(!$event->registrationEnded())
                                            @if (!Auth::check())

                                                <a href="{{ route('login') }}" class="btn btn-outline-success pull-left">S'inscrire</a>

                                            @elseif ($event->participates(Auth::user()) || $event->inWaitingList(Auth::user()))

                                                <div class="d-flex align-content-center flex-wrap">
                                                    @if ($event->team())
                                                        <a href="{{ $event->teamRoute(Auth::user()) }}" class="btn btn-outline-primary pull-left mr-2">Mon équipe</a>
                                                    @else
                                                        <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                                    @endif
                                                </div>

                                            @else
                                                <a href="{{ $event->registrationRoute() }}" class="btn btn-outline-success pull-left">S'inscrire</a>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="text-right d-flex">
                                        @if ($event->team())
                                            <a href="{{ route('teams.showTeams', $event->id) }}" class="btn btn-outline-primary pull-left mr-1">Voir les équipes</a>
                                        @else
                                            <a href="{{ route('event.showParticipants', $event->id) }}" class="btn btn-outline-primary pull-left mr-1">Voir les inscrits</a>
                                        @endif
                                        <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <h2 class="mt-4 text-center page-title">L'HISTORIQUE DE NOS ÉVÉNEMENTS</h2>

        <div class="mt-4" style="background: white">
            <div class="row">
                @foreach ($pastEvent as $event)
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                            <div class="card-body">
                                <div class="card-title">
                                <div>
                                    {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                </div>
                                    <h5 class="card-title">{{ $event->name }}</h5>
                                </div>
                                <p class="card-text">
                                    {!! nl2br($event->description_excerpt) !!}
                                </p>
                                <div class="text-right">
                                    <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
