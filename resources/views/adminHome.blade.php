@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="d-flex flex-wrap text-center">
            <a href="{{ route('eventManagement') }}" class="list-group-item list-group-item-action list-group-item-success col-4">Gérer les événements</a>
            <a href="{{ route('teams.selectEvent') }}" class="list-group-item list-group-item-action list-group-item-warning col-4">Les teams</a>
            <a href="{{ route('invoice') }}" class="list-group-item list-group-item-action list-group-item-success col-4">Générer une facture</a>
            <a href="#" class="list-group-item list-group-item-action list-group-item-info col-4">Liste des factures</a>
            <a href="#" class="list-group-item list-group-item-action list-group-item-success col-4">Ajouter un partenaire</a>
            <a href="{{ route('partnerList') }}" class="list-group-item list-group-item-action list-group-item-info col-4">Liste des partenaires</a>
            {{-- <a href="{{ route('news.new') }}" class="list-group-item list-group-item-action list-group-item-success col-4">Générer une new</a> --}}
            <a href="{{ route('permission') }}" class="list-group-item list-group-item-action list-group-item-danger col-4">Changer les droits administrateurs</a>
        </div>
    </div>
@endsection