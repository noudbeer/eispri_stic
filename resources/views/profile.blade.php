@extends('layouts.app')

@section('content')
    <div class="container md-4 pb-2" style="background:white">
        <a class="" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onfocus="this.blur();">
            <img class="cover-picture" src="{{ asset($user->getCoverPath()) }}">
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#cover_view">
                Voir l'image
            </button>

            @if ($editable)
                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#cover_upload">
                    Changer la photo de couverture
                </button>
            @endif
        </div>

        <div>
            <div class="d-flex">
                <a class="" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onfocus="this.blur();">
                    <img alt="{{ $user->lastname }} {{ $user->firstname }}" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail ml-3 avatar-profile">
                </a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#avatar_view">
                        Voir l'image
                    </button>

                    @if ($editable)
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#avatar_upload">
                            Changer la photo de profil
                        </button>
                    @endif
                </div>

                <h2 class="ml-2 mt-2">{{ $user->firstname }} {{ $user->lastname }}</h2>

                @if(Auth::check() && Auth::user()->hasRole('user'))
	                <div>
	                	<a href="{{ route('/') }}#join" class="btn btn-outline-primary pull-right" style="margin-top:10px;margin-left:20px;">Nous rejoindre</a>
	                </div>
                @endif
            </div>

            <div class="list-inline div-list-profile mt-5">
                <ul class="d-flex justify-content-around text-center">
                    <li class="list-inline-item">
                        <a href="#" class="link-liste-profile">
                            <div class="elem-title-profile">Évènements participés</div>
                            <div class="value">{{ $nb_registrations_passed }}</div>
                        </a>
                    </li>

                    <li class="list-inline-item">
                        <a href="#" class="link-liste-profile">
                            <div class="elem-title-profile">Évènements inscrits</div>
                            <div class="value">{{ $nb_next_registrations }}</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div>
            @if($nb_next_registrations > 0)
                <div>
                    <h5 class="text-center profile-title">PROCHAINS ÉVÈNEMENTS</h5>
                    <div class="row justify-content-center">
                        @foreach ($next_registrations as $event)
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                                <div class="card-body">
                                    <div class="card-title">
                                    <div>
                                        {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                    </div>
                                        <h5 class="card-title">{{ $event->name }}</h5>
                                    </div>
                                    <p class="card-text">
                                        {!! nl2br($event->description_excerpt) !!}
                                    </p>

                                    <div id="#registration" class="mt-3 d-flex justify-content-between">
                                        <div class="text-left d-flex">
                                            {{--
                                            @if(!$event->registrationEnded())
                                                @if (!Auth::check())

                                                    <a href="{{ route('login') }}" class="btn btn-outline-success pull-left">S'inscrire</a>

                                                @elseif ($event->participates(Auth::user()))

                                                    <div class="d-flex align-content-center flex-wrap">
                                                        <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                                    </div>

                                                @elseif ($event->inWaitingList(Auth::user()))

                                                    <div class="d-flex align-content-center flex-wrap">
                                                        <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                                    </div>

                                                @else
                                                    <a href="{{ $event->registrationRoute() }}" class="btn btn-outline-success pull-left">S'inscrire</a>
                                                @endif
                                            @endif
                                            --}}
                                        </div>
                                        <div class="text-right">
                                            <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endisset

            @if($nb_registrations_passed > 0)
                <div>
                    <h5 class="mt-4 text-center profile-title">ÉVÈNEMENTS PARTICIPÉS</h5>
                    <div class="row justify-content-center">
                        @foreach ($registrations_passed as $event)
                            <div class="col-md-4">
                                <div class="card mb-4">
                                    <img src="{{ asset("storage/img/events/$event->id") }}" class="card-img-top" alt="{{ $event->name }}">
                                    <div class="card-body">
                                        <div class="card-title">
                                        <div>
                                            {{ date('d/m/Y - H\hi', strtotime($event->beginning)) }}
                                        </div>
                                            <h5 class="card-title">{{ $event->name }}</h5>
                                        </div>
                                        <p class="card-text">
                                            {!! nl2br($event->description_excerpt) !!}
                                        </p>

                                        <div id="#registration" class="mt-3 d-flex justify-content-between">
                                            <div class="text-left d-flex">
                                                {{--
                                                @if(!$event->registrationEnded())
                                                    @if (!Auth::check())

                                                        <a href="{{ route('login') }}" class="btn btn-outline-success pull-left">S'inscrire</a>

                                                    @elseif ($event->participates(Auth::user()))

                                                        <div class="d-flex align-content-center flex-wrap">
                                                            <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                                        </div>

                                                    @elseif ($event->inWaitingList(Auth::user()))

                                                        <div class="d-flex align-content-center flex-wrap">
                                                            <a href="{{ $event->unregistrationRoute() }}" class="btn btn-outline-danger pull-left mr-2">Se désinscrire</a>
                                                        </div>

                                                    @else
                                                        <a href="{{ $event->registrationRoute() }}" class="btn btn-outline-success pull-left">S'inscrire</a>
                                                    @endif
                                                @endif
                                                --}}
                                            </div>
                                            <div class="text-right">
                                                <a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-primary pull-right">Consulter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endisset
        </div>
    </div>


{{-- --------------------------------------------------------------------------------------------------------------------------------------------------- --}}
{{-- Les modals --}}

    <div class="modal" tabindex="-1" role="dialog" id="avatar_view">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Photo de profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <img alt="{{ $user->lastname }} {{ $user->firstname }}" src="{{ asset($user->getAvatarPath()) }}" style="max-width: 100%">
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="avatar_upload">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Changer la photo de profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('changeAvatar') }}" class="form-signin mt-3">
                        @csrf
                        <div class="form-group">
                            <div class="custom-file">
                                <label class="custom-file-label" for="inputFileAvatar" id="label-avatar" name="label-avatar">Photo de profil</label>
                                <input name="avatar" type="file" class="custom-file-input @error('avatar') is-invalid @enderror" aria-describedby="inputGroupFileAddon01"  id="avatar" onchange="changeNameFile('avatar', 'label-avatar')" required>
                            </div>
                            @error('avatar')
                                <p class="" role="alert">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-success">Enregister</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="cover_view">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Photo de couverture</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <img alt="{{ $user->lastname }} {{ $user->firstname }}" src="{{ asset($user->getCoverPath()) }}" style="max-width: 100%">

                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="cover_upload">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Changer la photo de couverture</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('changeCover') }}" class="form-signin mt-3">
                        @csrf
                        <div class="form-group">
                            <div class="custom-file">
                                <label class="custom-file-label" for="inputFileCover" id="label-cover" name="label-cover">Photo de couverture</label>
                                <input name="cover" type="file" class="custom-file-input @error('cover') is-invalid @enderror" aria-describedby="inputGroupFileAddon01"  id="cover" onchange="changeNameFile('cover', 'label-cover')" required>
                            </div>
                            @error('cover')
                                <p class="" role="alert">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-success">Enregister</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/changeNameFile.js') }}"></script>
@endsection
