@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container md-4 pb-2" style="background:white">
            <h2 class="mt-4 text-center page-title">PARAMÈTRES DU COMPTE<h2>
            <form method="POST" enctype="multipart/form-data" action="{{ route('changeInfo') }}" class="form-signin mt-3">
                @csrf

                <label for="firstname" class="sr-only">Prénom</label>
                <div>
                    <input type="text" id="firstname" name="firstname" class="form-control mt-3" placeholder="{{ $user->firstname }}">
                </div>

                <label for="lastname" class="sr-only">Nom</label>
                <div>
                    <input type="text" id="lastname" name="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror mt-3" placeholder="{{ $user->lastname }}">
                    @error('lastname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <select class="custom-select mt-3" id="study" name="study">
                    @foreach ($studies as $study)
                        @if($study->id == $user->study_id)
                            <option value="{{ $study->id }}" selected="true">{{ $study->name }}</option>
                        @else
                            <option value="{{ $study->id }}">{{ $study->name }}</option>
                        @endif
                    @endforeach
                </select>

                <h5 class="mt-5">Modification de l'adresse e-mail</h5>
                <label for="new_email" class="sr-only">Nouvelle adresse e-mail</label>
                <div>
                    <input id="new_email" name="new_email" type="email" class="form-control mt-3 @error('new_email') is-invalid @enderror" placeholder="{{ $user->email }}">
                    @error('new_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <label for="new_email_confirmation" class="sr-only">Nouvelle adresse e-mail (confirmation)</label>
                <div>
                    <input id="new_email_confirmation" name="new_email_confirmation" type="email" class="form-control mt-3 @error('new_email_confirmation') is-invalid @enderror" placeholder="Nouvelle adresse e-mail (confirmation)">
                    @error('new_email_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <h5 class="mt-5">Modification du mot de passe</h5>
                <label for="new_password" class="sr-only">Nouveau mot de passe</label>
                <div>
                    <input id="new_password" name="new_password" type="password" class="form-control mt-3 @error('new_password') is-invalid @enderror" placeholder="Nouveau mot de passe">
                    @error('new_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <label for="new_password_confirmation" class="sr-only">Nouveau mot de passe (confirmation)</label>
                <div>
                    <input id="new_password_confirmation" name="new_password_confirmation" type="password" class="form-control mt-3" placeholder="Nouveau mot de passe (confirmation)">
                    @error('new_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                {{-- <div class="custom-control custom-switch mt-3">
                    <input type="checkbox" class="custom-control-input" id="newsletter">
                    <label class="custom-control-label" for="newsletter">S'inscrire à la newsletter</label>
                </div> --}}

                <div class="mt-5">
                    <h5 class="text-center">Valider les informations</h5>
                    <div class="d-flex align-items-center">
                        <input id="password" name="password" type="password" class="form-control mt-3 mr-1" placeholder="Entrez votre mot de passe pour confirmer les changements" required>
                        <button class="btn btn-lg btn-primary btn-block mt-3 ml-1" type="submit">Changer ses informations</button>
                    </div>
                    @if(\Session::has('error'))
                        <div class="alert alert-danger text-center mt-1">
                            {!! \Session::get('error') !!}
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>

    <script src="{{ asset('js/changeNameFile.js') }}"></script>
@endsection
