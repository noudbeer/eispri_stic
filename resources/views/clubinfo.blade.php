@extends('layouts.app')
@section('content')
	<div class="container mt-4 mb-4 pb-2" style="background:white">
		<h2 class="mt-4 text-center page-title">CLUB INFO</h2>
		<div class="card mt-4">
			<div class="mt-2 ml-3">
				<h2 class="strong">Godot</h1>
			</div>
			<div class="container mt-3 ml-3">
				<h2>Liens :</h2>
				<ul>
					<li>
						<a href="https://godotengine.org/download">Télécharger Godot</a>
					</li>
					<li>
						<a href="https://docs.godotengine.org/fr/stable/">Documentation française</a>
					</li>
				</ul>
			</div>

			<div class="container ml-3">
				<h2>Cours :</h2>
				<ul>
					<li>
						<a href="https://eispri-stic.gitlab.io/tuto-godot-1-flappybird-sujet">Tuto Flappy Bird</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
@endsection