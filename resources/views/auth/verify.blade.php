@extends('layouts.app')

@section('content')
<div class="container mt-2">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Vérification de l'adresse email</div>

				<div class="card-body">
					@if (session('resent'))
						<div class="alert alert-success" role="alert">
							Un nouveau lien de confirmation a été envoyé à l'adresse indiquée.
						</div>
					@endif

					Avant de continuer, veuillez cliquer sur le lien de confirmation qui vous a été envoyé par mail.
					Si vous ne l'avez pas reçu,
					<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
						@csrf
						<button type="submit" class="btn btn-link p-0 m-0 align-baseline">cliquez ici pour en renvoyer un</button>.
					</form>

					<br><br>
					Si vous vous êtes trompé en tapant votre adresse e-mail, vous pouvez la changer dans <a href="{{ route('settings') }}">les paramètres du compte</a>.

					<br><br>
					Si malgré tout vous n'arrivez toujours pas à activer votre compte, <a href="{{ route('contact') }}">contactez-nous directement</a>.
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
