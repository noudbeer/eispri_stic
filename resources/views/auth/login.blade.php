@php
	$is_login = Route::current()->getName() == 'login';
	$is_register = Route::current()->getName() == 'register';
@endphp

@extends('layouts.app')

@section('content')
	<div class="d-flex justify-content-around container mt-4 md-4 pt-4" style="background: white">
		<div>
			<h2>eispri-stic.fr</h2>
			<h5 class="mt-4">Pourquoi s'inscrire ?</h5>
			<p>
				<ul>
					<li>Rester informé de la vie étudiante</li>
					<li>S'inscrire aux évènements organisés par l'association</li>
					<li>Entrer en contact avec les autres étudiants de la promo</li>
				</ul>
			</p>
		</div>

		<div>
			<nav>
				<div class="nav nav-tabs justify-content-center" id="tab" role="tablist">
					<a class="nav-item nav-link @if ($is_login) active @endif" id="nav-login-tab" data-toggle="tab" href="#login-pane" role="login" aria-controls="login-pane" aria-selected="@if ($is_login) true @else false @endif">S'identifier</a>
					<a class="nav-item nav-link @if ($is_register) active @endif" id="nav-register-tab" data-toggle="tab" href="#register-pane" role="register" aria-controls="register-pane" aria-selected="@if ($is_register) true @else false @endisset">S'enregister</a>
				<div>
			</nav>


			<div class="tab-content" id="TabContent">
				{{-- Login --}}

				<div class="tab-pane fade @if ($is_login) show active @endif" id="login-pane" role="tabpanel" aria-labelledby="login-pane">
					<form method="post" action="{{ route('login') }}" class="form-signin mt-3 mb-3">
						@csrf
						<h3 class="text-center">Connexion</h3>

						<div>
							<label for="email" class="sr-only">Email</label>
							<input
								type="email"
								id="email"
								name="email"
								class="form-control mt-3 @error('email') is-invalid @enderror"
								placeholder="Email"
								value="{{ old('email') }}"
								autocomplete="email"
								required
								@if ($is_login) autofocus @endif
							>

							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div>
							<label for="password" class="sr-only">Mot de passe</label>
							<input
								type="password"
								id="password"
								name="password"
								class="form-control mt-3 @error('password') is-invalid @enderror"
								placeholder="Mot de passe"
								autocomplete="current-password"
								required
							>

							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						{{--
						<div class="form-check mt-2">
							<input
								type="checkbox"
								id="remember"
								name="remember"
								{{ old('remember') ? 'checked' : '' }}
							>
							<label class="form-check-label" for="remember">Rester connecté</label>
						</div>
						--}}
						<div class="mt-3"></div>

						<button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>

						@if (Route::has('password.request'))
							<a class="btn btn-link" href="{{ route('password.request') }}">
								Mot de passe oublié ?
							</a>
						@endif
					</form>
				</div>


				{{-- Register --}}

				<div class="tab-pane fade @if ($is_register) show active @endif" id="register-pane" role="tabpanel" aria-labelledby="register-pane">
					<form method="post" action="{{ route('register') }}" class="form-signin mt-3 mb-3">
						@csrf
						<h3 class="text-center">Inscription</h3>

						<label for="reg_firstname" class="sr-only">Prénom</label>
						<div>
							<input
								type="text"
								id="reg_firstname"
								name="reg_firstname"
								class="form-control @error('reg_firstname') is-invalid @enderror mt-3"
								placeholder="Prénom"
								value="{{ old('reg_firstname') }}"
								autocomplete="given-name"
								required
								@if ($is_register) autofocus @endif
							>

							@error('reg_firstname')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<label for="reg_lastname" class="sr-only">Nom</label>
						<div>
							<input
								type="text"
								id="reg_lastname"
								name="reg_lastname"
								class="form-control @error('reg_lastname') is-invalid @enderror mt-3"
								placeholder="Nom"
								value="{{ old('reg_lastname') }}"
								autocomplete="family-name"
								required
							>

							@error('reg_lastname')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<select name="reg_study" class="custom-select mt-3">
							@foreach ($studies as $study)
								<option value="{{ $study->id }}">{{ $study->name }}</option>
							@endforeach
						</select>

						<label for="reg_email" class="sr-only">E-mail</label>
						<div>
							<input
								type="email"
								id="reg_email"
								name="reg_email"
								class="form-control mt-3 @error('reg_email') is-invalid @enderror"
								placeholder="E-mail (USMB recommandé)"
								value="{{ old('reg_email') }}"
								autocomplete="email"
								required
							>
							@error('reg_email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror

							<p class="alert alert-warning mt-1">Une adresse e-mail de l'USMB sera nécessaire pour participer à certains événements.</p>
						</div>

						<label for="reg_email_confirmation" class="sr-only">E-mail (confirmation)</label>
						<div>
							<input
								type="email"
								id="reg_email_confirmation"
								name="reg_email_confirmation"
								class="form-control mt-3 @error('reg_email_confirmation') is-invalid @enderror"
								placeholder="E-mail (confirmation)"
								required
							>
						</div>

						<label for="reg_password" class="sr-only">Mot de passe</label>
						<div>
							<input
								type="password"
								id="reg_password"
								name="reg_password"
								class="form-control mt-3 @error('reg_password') is-invalid @enderror"
								placeholder="Mot de passe"
								autocomplete="new-password"
								required
							>

							@error('reg_password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<label for="reg_password_confirmation" class="sr-only">Mot de passe (confirmation)</label>
						<div>
							<input
								type="password"
								id="reg_password_confirmation"
								name="reg_password_confirmation"
								class="form-control mt-3 @error('reg_password_confirmation') is-invalid @enderror"
								placeholder="Mot de passe (confirmation)"
								required
							>
						</div>

						<div class="checkbox mt-3">
							<input type="checkbox" id="reg_newsletter" name="reg_newsletter" checked>
							<label for="reg_newsletter">S'inscrire à la newsletter</label>
						</div>

						<button class="btn btn-lg btn-primary btn-block mt-3" type="submit">S'enregister</button>
					</form>
				</div>
			</div>
		<div>
	</div>
@endsection
