<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'EISPRI STIC') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/invoice.css') }}" rel="stylesheet">
</head>

<body class="page">
    <div class="container md-4 p-3" style="background:white">

        <div class="d-flex justify-content-between">
            <div class="text-align-center">    
                <h1>Association EISPRI STIC</h1>
                <p>asso.eispri.stic@gmail.com</p>

                <p>
                    <div>Bâtiment Tarentaise B010</div>
                    <div>Université Savoie Mont Blanc</div>
                    <div>73370 Le Bourget-du-Lac</div>
                </p>
            </div>

            <img class="logo_site" src="{{ asset("storage/img/logo.png") }}" alt="logo" width="15%"/>
        </div>

        <div class="dropdown-divider py-2"></div>

        <div class="d-flex justify-content-between">
            <div>
                <h2>Facturation à l'attention de</h2>
                <div class="dropdown-divider py-2"></div>
                <div>
                    <div>{{ $name }}</div>
                    <div>{{ $address }}</div>
                    <div>{{ $email }}</div>
                </div>
            </div>
            <div>
                <div>Facture n°{{ strftime('%Y-%m',strtotime($now)) }}-xxx</div>
                <div>Le {{ strftime('%d/%m/%Y',strtotime($now)) }}</div>
            </div>
        </div>

        <table class="table table-bordered mt-5 mb-5">
            <thead>
                <tr>
                <th scope="col">Désignation</th>
                <th scope="col">Montant</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>{{ $subject }}</td>
                <td>{{ $price }}€</td>
                </tr>
            </tbody>
        </table>
        
        <div class="d-flex justify-content-around">
            <div>
                <p>
                    En votre aimable règlement,<br>
                    Cordialement,
                </p>
                <p> L'association EISPRI STIC</p>
            </div>

            <div style="border-left: solid 1px #e9ecef"></div>

            <div>
                <h4>Conditions et modalités de paiement</h4>
                <div class="dropdown-divider py-2"></div>
                <p>Le paiement est dû avant le {{ strftime('%d/%m/%Y',strtotime($deadline)) }}</p>
                <div>Crédit Agricole</div>
                <div>IBAN : FR76 1810 6008 1096 7342 4485 336</div>
                <div>BIC/Swift : AGRIFRPP881</div>
            </div>
        </div>

    </div>
</body>
    