@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container md-4" style="background:white">
        <h2 class="mt-4 text-center page-title">NOTRE BOUTIQUE</h2>
        <div class="text-center">
            <div class="alert alert-warning" role="alert">
                Pas d'articles en vente pour le moment.
            </div>
        </div>
    </div>
</div>
@endsection
