@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mt-4 pb-4" style="background: white">
            <h2 class="mt-4 text-center page-title">{{ $event->name }}</h2>
            <div class="row justify-content-center p-2">
                <ul>
                    @foreach($participants as $user)
                        <li class="mb-2">
                            <a href="{{ route('profile', $user->id) }}"><img alt="Avatar" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;"></a>
                            <a href="{{ route('profile', $user->id) }}">{{ $user->firstname }} {{ $user->lastname }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection