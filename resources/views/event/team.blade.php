@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <h1 class="mt-1 text-center page-title">Équipe {{ $team->name }}</h1>
        <div class="card-body">
            @if ($team->isOwner(Auth::user()))
                <p class="text-center"><em class="text-center">Vous êtes le chef de cette équipe.</em></p>
            @endif

            <div class="row justify-content-around">
                <div>
                    <p>
                        Membres de votre équipe :
                        <ul>
                            @foreach ($membersTeam as $user)
                                <li>
                                    <a href="{{ route('profile', $user->id) }}"><img alt="Avatar" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;"></a>
                                    <a href="{{ route('profile', $user->id) }}">{{ $user->firstname }} {{ $user->lastname }}</a>@if($team->isOwner($user)) (chef d'équipe)@endif
                                </li>
                            @endforeach
                        </ul>
                    </p>
                </div>

                <div>
                    <p class="text-center">Ajouter un membre :</p>
                    @if($nb_membersTeam < $membresMax)
                        <form class="d-flex justify-conntent-between" action="{{ route('searchMate', $team->id) }}" method="POST">
                            @csrf
                            <input class="form-control mr-1" type="search" placeholder="Ajouter un équipier" aria-label="Search" name="searchBar">
                            <button class="btn btn-success ml-1" type="submit">Rechercher</button>
                        </form>
                        @isset($results)
                            <ul class="list-group mt-1">
                                @foreach ($results as $res)
                                    <li class="list-group-item list-group-item-primary d-flex justify-content-around align-items-center text-center">
                                        <a href="{{ route('profile', $res->id) }}"><img alt="Avatar" src="{{ asset($res->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;"></a>
                                        <a href="{{ route('profile', $res->id) }}">{{ $res->firstname }} {{ $res->lastname }}</a>

                                        <div>
                                            <a class="btn btn-success" href="{{ route('AddMate', array($team->id, $res->id)) }}">Ajouter</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endisset
                    @else
                        <p><em>Votre équipe est déjà pleine.</em></p>
                    @endif

                    <a class="btn btn-danger mt-4" href="{{ route('event.unregister', $team->event->id) }}">Quitter l'équipe et se désinscrire de l'évènement</a>
                    @if ($team->isOwner(Auth::user()))
                        <p class="alert alert-danger mt-1">Vous êtes le chef de l'équipe :<br>en vous désinscrivant, vous supprimerez l'équipe<br>et désinscrirez tous ses membres !</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
