@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <div class="card-body">
            <p>Vous avez été désinscrit de l'évènement « <strong>{{ $event->name }}</strong> » !</p>

            <p class="text-center mt-4">
                <a href="{{ route('event.show', $event->id) }}#registration" class="btn btn-outline-primary">Revenir</a>
            </p>
        </div>
    </div>
</div>
@endsection
