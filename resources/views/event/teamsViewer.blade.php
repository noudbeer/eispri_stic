@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mt-4 pb-4" style="background: white">
            <h2 class="mt-4 text-center page-title">{{ $event->name }}</h2>
            <div class="row justify-content-center p-2">
                @foreach($teams as $team)
                    <div class="col-md-3">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3>{{ $team->name }}</h3>
                                    <ul>
                                        @foreach($team->users_by_registration_order as $user)
                                            <li>
                                                <a href="{{ route('profile', $user->id) }}"><img alt="Avatar" src="{{ asset($user->getAvatarPath()) }}" class="img-thumbnail img-fluid rounded-circle" width="50px;"></a>
                                                <a href="{{ route('profile', $user->id) }}">{{ $user->firstname }} {{ $user->lastname }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection