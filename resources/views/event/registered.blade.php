@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <div class="card-body">
            <p>Vous avez été inscrit à l'évènement « <strong>{{ $event->name }}</strong> »
            @if (isset($team) && $team !== null)
                dans l'équipe « <strong>{{ $team->name }}</strong> »
            @endif
            !</p>

            @if (isset($team) && $team !== null)
                <p>Vous pouvez inviter d'autres membres à rejoindre votre équipe à partir du bouton « Mon équipe » en bas de la page de l'évènement.</p>
            @endif

            <p>Pensez à consulter vos e-mails, nous y enverrons toutes les nouvelles informations concernant l'évènement.<br>
            Si vous avez des questions, vous pouvez rejoindre <a href="https://discord.gg/gRSS9hH">notre serveur Discord</a>. Nous y postons aussi toutes les news en rapport avec nos évènements.</p>

            <p class="text-center mt-4">
                <a href="{{ route('event.show', $event->id) }}#registration" class="btn btn-outline-primary">Revenir</a>
            </p>
        </div>
    </div>
</div>
@endsection
