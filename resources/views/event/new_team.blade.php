@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <h1 class="card-title">Inscription d'une nouvelle équipe</h1>

        <div class="card-body">
            <p>Vous allez vous inscrire en créant une nouvelle équipe. Vous pourrez par la suite inviter les personnes de votre choix à la rejoindre.</p>
            <p>Si vous désirez rejoindre l'équipe qu'un de vos amis a crée, vous n'êtes pas au bon endroit : demandez-lui plutôt de vous inviter !<br>Vous recevrez alors un mail confirmant votre ajout à son équipe.</p>
            <br>

            <form method="post" action="{{ route('event.new_team') }}">
                @csrf

                <input type="hidden" name="event_id" value="{{ $event->id }}">

                <p class="text-center"><label for="name">Nom de l'équipe :</label> <input type="text" id="name" name="name"></p>

                <p class="text-center"><input type="submit" class="btn btn-outline-primary" value="Valider"></p>
            </form>
        </div>
    </div>
</div>
@endsection
