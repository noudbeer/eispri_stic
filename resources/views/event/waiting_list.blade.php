@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-3 mb-4">
        <div class="card-body">
            <p>En raison d'un trop grand nombre de participants pour l'évènement « <strong>{{ $event->name }}</strong> », vous avez été placé sur liste d'attente.</p>

            <p>Cependant, pas de soucis ! Deux options s'offrent à vous :</p>
            <ul>
                <li>Si un participant se désiste, vous serez automatiquement inscrit à sa place.</li>
                <li>Si l'évènement suscite une trop forte demande, nous étudieront la situation et feront en sorte d'ouvrir des places supplémentaires, ou de créer un évènement annexe.</li>
            </ul>

            <p>Surveillez vos e-mails. Dès lors que la situation évoluera, nous vous contacterons par ce biais.</p>

            <p>Si vous avez la moindre question, n'hésitez pas à venir la poser sur <a href="https://discord.gg/gRSS9hH">notre serveur Discord</a> ou <a href="{{ route('contact') }}">par e-mail</a>. Nous serons ravis d'y répondre !</p>

            <p class="text-center mt-4">
                <a href="{{ route('event.show', $event->id) }}#registration" class="btn btn-outline-primary">Revenir</a>
            </p>
        </div>
    </div>
</div>
@endsection
