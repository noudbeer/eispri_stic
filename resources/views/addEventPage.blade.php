@extends('layouts.app')

@section('content')
    <div class="container md-4 pb-3" style="background: white;">
        @isset($event)
            <h2 class="mt-4 text-center page-title">MODIFIER UN ÉVÉNEMENT</h2>
        @else
            <h2 class="mt-4 text-center page-title">AJOUTER UN ÉVÉNEMENT</h2>
        @endisset

        <form class="text-center" enctype="multipart/form-data" action="{{ route('addEvent') }}" method="POST">
            @csrf

            <input type="hidden" name="id" id="id" value="@isset($event) {{ $event->id }} @endisset" />
            @error('id')
                <p class="" role="alert">
                    <strong>{{ $message }}</strong>
                </p>
            @enderror

            <div class="form-group">
                <label for="title">Titre</label>
                <input type="title" class="form-control text-center @error('title') is-invalid @enderror" id="title" name="title" @isset($event) value="{{ $event->name }}" @endisset required>
                @error('title')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <div class="custom-file">
                    <label class="custom-file-label" for="inputFileImage" id="label-image" name="label-image">Image</label>
                    <input name="image" type="file" class="custom-file-input @error('image') is-invalid @enderror" aria-describedby="inputGroupFileAddon01" id="image" onchange="changeNameFile('image', 'label-image')">
                </div>
                @error('image')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="description_excerpt" name="label-description_excerpt">Brève description</label>
                <textarea class="form-control @error('description_excerpt') is-invalid @enderror" id="description_excerpt" name="description_excerpt">@isset($event){{ $event->description_excerpt }}@endisset</textarea>
                @error('description_excerpt')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <script src="https://uicdn.toast.com/editor/latest/toastui-editor-all.min.js"></script>
            <div class="form-group">
                <label for="description-editor" name="label-description">Description</label>
                <div id="description-editor"></div>
                @error('description')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror

                <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" style="display: none;">@isset($event){{ $event->description }}@endisset</textarea>

                <script>
                    const description = document.querySelector("#description");

                    const editor = new toastui.Editor({
                        el:               document.querySelector("#description-editor"),
                        height:           "400px",
                        previewStyle:     "vertical",
                        initialEditType:  "markdown",
                        usageStatistics:  false,

                        events:           {
                            change:  () => {
                                description.value = editor.getHTML();
                            }
                        }
                    });
                    editor.setHTML(description.value);
                </script>
            </div>

            <div class="form-group">
                <label for="datetimeStart">Début</label>
                <div>
                    <input type="date" id="date-start" name="date-start" class="@error('date-start') is-invalid @enderror" @isset($event) value="{{ $event->beginning->format("Y-m-d") }}" @endisset>
                    <input type="time" id="time-start" name="time-start" class="@error('time-start') is-invalid @enderror" @isset($event) value="{{ $event->beginning->format("H:i") }}" @endisset>
                </div>
                @error('date-start')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
                @error('time-start')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="datetimeEnd">Fin</label>
                <div>
                <input type="date" id="date-end" name="date-end" class="@error('date-end') is-invalid @enderror" @isset($event) value="{{ $event->end->format("Y-m-d") }}" @endisset>
                    <input type="time" id="time-end" name="time-end" class="@error('time-end') is-invalid @enderror" @isset($event) value="{{ $event->end->format("H:i") }}" @endisset>
                </div>

                @error('date-end')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
                @error('time-end')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="datetime">Fermeture des inscriptions</label>
                <div>
                    <input type="date" id="inscription-date-end" name="inscription-date-end" class="@error('inscription-date-end') is-invalid @enderror" @isset($event) value="{{ $event->registration_end->format("Y-m-d") }}" @endisset>
                    <input type="time" id="inscription-time-end" name="inscription-time-end" class="@error('inscription-time-end') is-invalid @enderror" @isset($event) value="{{ $event->end->format("H:i") }}" @endisset>
                </div>
                @error('inscription-date-end')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
                @error('inscription-time-end')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="room">Nombre de participants maximum<br><em>Laisser vide pour ne pas limiter le nombre de participants</em></label>
                <input type="number" class="form-control text-center @error('room') is-invalid @enderror" id="room" name="room" @isset($event) value="{{ $event->room }}" @endisset>
                @error('room')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <div>
                    <label for="squad">Inscription par équipe</label>
                    <input type="checkbox" aria-label="Checkbox" class="ml-3" id="myCheck" onclick="showDiv()">
                </div>

                <div id="elem" style="display: none;">
                    <label for="team_size">Nombre de personnes par équipe</label>
                    <input type="number" class="form-control text-center" id="team_size" name="team_size" aria-label="Nombre de personne par équipe" @isset($event) value="{{ $event->team_size }}" @endisset>
                    @error('team_size')
                        <p class="" role="alert">
                            <strong>{{ $message }}</strong>
                        </p>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label for="localisation">Lieu</label>
                <input type="control" class="form-control text-center @error('localisation') is-invalid @enderror" id="localisation" name="localisation" @isset($event) value="{{ $event->location }}" @endisset>
                @error('localisation')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            <div class="form-group">
                <label for="price">Prix<br><em>Laisser à 0 pour rendre l'évènement gratuit</em></label>
                <input type="number" class="form-control text-center @error('price') is-invalid @enderror" id="price" name="price" @isset($event) value="{{ $event->price }}" @else value="0" @endisset>

                @error('price')
                    <p class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </p>
                @enderror
            </div>

            @isset($event)
                <button class="btn btn-success mt-2">MODIFIER L'ÉVÉNEMENT</button>
            @else
                <button class="btn btn-success mt-2">AJOUTER L'ÉVÉNEMENT</button>
            @endisset
        </form>
    </div>

    <script src="{{ asset('js/changeNameFile.js') }}"></script>
    <script src="{{ asset('js/checkbox.js') }}"></script>
@endsection
