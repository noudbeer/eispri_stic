<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'EISPRI STIC') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/page.css') }}" rel="stylesheet">
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">

    <!-- Éditeur visuel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/codemirror.min.css" />
    <link rel="stylesheet" href="https://uicdn.toast.com/editor/latest/toastui-editor.min.css" />

    <!-- Icone -->
    <link rel="icon" type="image/x-icon" href="{{ asset("storage/img") }}/logo.ico" /><link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <!--  -->
</head>

<body>
    <div id="app" class="header-nav">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand"  onfocus="this.blur();" href="{{ url("/") }}">
                <img class="logo_site" src="{{ asset("storage/img/logo.png") }}" alt="logo"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto text-center">
                    <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route("/") }}">ACCUEIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route("events.index") }}">ÉVÉNEMENTS</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route ("shop") }}">BOUTIQUE</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route("contact") }}">CONTACT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route("clubinfo") }}">CLUB INFO</a>
                    </li>


                    @if(Auth::check() && (Auth::user()->hasRole('admin') || Auth::user()->hasRole('superadmin')))
                        <li class="nav-item">
                        <a class="nav-link text-dark" onfocus="this.blur();" href="{{ route('adminHome') }}">INTERFACE ADMIN</a>
                        </li>
                    @endif
                </ul>

                @guest
                    {{-- Si l'utilisateur n'est pas connecté --}}
                    <div class="d-flex flex-row-reverse">
                        <a class="btn btn-outline-dark" href="{{ route('register') }}" role="button">S'enregister</a>
                        &nbsp;
                        <a class="btn btn-outline-dark" href="{{ route('login') }}" role="button">S'identifier</a>
                    </div>

                @else
                    {{-- Si l'utilisateur est connecté --}}
                    <div class="d-flex flex-row-reverse">
                        <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onfocus="this.blur();">
                            <?php $user = Auth::user(); ?>
                            <img src="{{ asset($user->getAvatarPath()) }}" class="img-cercle">
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('my_profile') }}">Profil</a>
                            <a class="dropdown-item" href="{{ route('settings') }}">Paramètres du compte</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route("logout") }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{-- {{ __('Logout') }}  --}}
                                    Se déconnecter
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                @endguest
            </div>
        </nav>
    </div>

    <main>
        @yield('content')
    </main>

    <footer class="container col-6">
        <div class="dropdown-divider py-2"></div>

        <div class="row">
            <div class="col-12 col-md">
                <img class="logo" src="{{ asset("storage/img/logo.png") }}" alt="logo" width="100px"/>
            </div>

            <div class="col-6 col-md">
            <h5>EISPRI STIC</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ route('/') }}#EISPRI_STIC">Présentation</a></li>
                    <li><a class="text-muted" href="{{ route('/') }}#office">Bureau de l'association</a></li>
                    <li><a class="text-muted" href="{{ route('/') }}#members">Bénévoles</a></li>
                    <li><a class="text-muted" href="{{ route('/') }}#projects">Projets</a></li>
                    {{-- <li><a class="text-muted" href="{{ route('/') }}#parteners">Partenaires</a></li> --}}
                </ul>
            </div>

            <div class="col-6 col-md">
                <h5>Nos événements</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ route('events.index') }}">À venir</a></li>
                    <li><a class="text-muted" href="{{ route('events.index') }}">L'historique</a></li>
                </ul>
            </div>

            <div class="col-6 col-md">
                <h5>Contact</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ route('contact') }}">Nous contacter</a></li>
                    {{-- <li><a class="text-muted" href="#">Devenir partenaire</a></li> --}}
                </ul>
            </div>
        </div>

        <div>
            <div>
                <img src="https://infomaniak.com/img/common/logo-infomaniak.svg" alt="infomaniak" style="margin-right: 10px;">
            </div> 
            <div>
                Site hébergé par Infomaniak.
            </div>
        </div>

            <div class="d-flex justify-content-center">
                <a class="logo-social" onfocus="this.blur();" href="https://discord.gg/gRSS9hH"><img class="logo-social" src="{{ asset('storage/img/logo_discord.png') }}" onmouseover="this.src='{{ asset('storage/img/logo_discord:hover.png') }}'" onmouseout="this.src='{{ asset('storage/img/logo_discord.png') }}'" alt="discord"/></a></li>
                <a class="logo-social" onfocus="this.blur();" href="https://www.facebook.com/EispriSticTechnolac/"><img class="logo-social" src="{{ asset('storage/img/logo_facebook.png') }}" onmouseover="this.src='{{ asset('storage/img/logo_facebook:hover.png') }}'" onmouseout="this.src='{{ asset('storage/img/logo_facebook.png') }}'" alt="facebook"/></a></li>
                <a class="logo-social" onfocus="this.blur();" href="https://www.instagram.com/eispri.stic/"><img class="logo-social" src="{{ asset('storage/img/logo_instagram.png') }}" onmouseover="this.src='{{ asset('storage/img/logo_instagram:hover.png') }}'" onmouseout="this.src='{{ asset('storage/img/logo_instagram.png') }}'" alt="instagram"/></a></li>
                <a class="logo-social" onfocus="this.blur();" href="{{ asset('storage/img/snapchat.jpeg') }}"><img class="logo-social" src="{{ asset('storage/img/logo_snapchat.png') }}" onmouseover="this.src='{{ asset('storage/img/logo_snapchat:hover.png') }}'" onmouseout="this.src='{{ asset('storage/img/logo_snapchat.png') }}'" alt="snapchat"/></a></li>
            </div>

        <div class="d-flex bd-highlight mb-3">
            <div class="align-self-end mr-auto p-2 bd-highlight">
                <a href="{{ route('privacy') }}" class="text-danger">Confidentialité</a> | <a href="{{ route('legalNotice') }}" class="text-danger">Mentions Légales</a> | <?php echo date('Y'); ?> ©
            </div>
        </div>
    </footer>
</body>
</html>
