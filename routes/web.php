<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\EventController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\ProfileController;

Route::resource('/events', 'EventController');
Route::get('/event/{id}', 'EventController@show')
    ->name('event.show');
Route::get('/event/{id}/register', 'EventController@register')
    ->name('event.register')
    ->middleware('verified');
Route::get('/event/{id}/unregister', 'EventController@unregister')
    ->name('event.unregister')
    ->middleware('verified');
Route::get('/event/{id}/new-team', 'EventController@showNewTeamPage')
    ->name('event.show_new_team')
    ->middleware('verified');
Route::post('/event/new-team', 'EventController@registerNewTeam')
    ->name('event.new_team')
    ->middleware('verified');
Route::get('/event/team/{id}', 'EventController@showTeamPage')
    ->name('event.show_team')
    ->middleware('verified');
Route::post('/event/team/{id}', 'EventController@searchMate')
->name('searchMate')
    ->middleware('verified');
Route::get('event/team/{team_id}/addMate/{mate_id}', 'EventController@AddMate')
    ->name('AddMate')
    ->middleware('verified');



Route::get('/shop', function() {
    return view('shop');
})->name('shop');


Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/clubinfo', function () {
    return view('clubinfo');
})->name('clubinfo');


// Admin
Route::get('/admin', 'Admin\AdminHomeController@index')->name('adminHome')
    ->middleware('verified');

Route::get('/admin/permission', 'Admin\PermissionController@index')->name('permission')
    ->middleware('verified');
Route::post('/admin/permission', 'Admin\PermissionController@search')->name('permissionSearch')
    ->middleware('verified');
Route::get('/admin/permission/addAdmin/{id}', 'Admin\PermissionController@addAdmin')->name('addAdmin')
    ->middleware('verified');
Route::get('/admin/permission/addSuper/{id}', 'Admin\PermissionController@addSuperAdmin')->name('addSuperAdmin')
    ->middleware('verified');
Route::get('admin/permission/addMember/{id}', 'Admin\PermissionController@addMember')->name('addMember')
    ->middleware('verified');
Route::get('/admin/permission/delete/{id}', 'Admin\PermissionController@deleteAdmin')->name('deleteAdmin')
    ->middleware('verified');
Route::get('/admin/permission/delete/{id}', 'Admin\PermissionController@deleteRole')->name('deleteRole')
    ->middleware('verified');

Route::get('/admin/eventManagement', 'Admin\EventManagementController@index')->name('eventManagement')
    ->middleware('verified');
Route::get('/admin/eventManagement/addEventPage', 'Admin\EventManagementController@addEventPage')->name('addEventPage')
    ->middleware('verified');
Route::post('/admin/eventManagement/addEvent', 'Admin\EventManagementController@addEvent')->name('addEvent')
    ->middleware('verified');
Route::get('/admin/eventManagement/editEventPage/{id}', 'Admin\EventManagementController@addEventPage')->name('editEventPage')
    ->middleware('verified');
Route::get('/admin/eventManagement/deleteEvent/{id}', 'Admin\EventManagementController@deleteEvent')->name('deleteEvent')
    ->middleware('verified');

Route::get('/admin/teams/selectEvent', 'Admin\TeamsController@selectEvent')->name('teams.selectEvent');
Route::get('/teams/event/{id}', 'EventController@showTeams')->name('teams.showTeams')
    ->middleware('verified');
Route::get('/participants/event/{id}', 'EventController@showParticipants')->name('event.showParticipants')
    ->middleware('verified');


Route::get('/admin/invoice', 'Admin\InvoiceController@index')->name('invoice')
    ->middleware('verified');
Route::post('/admin/invoice/preview', 'Admin\InvoiceController@previewInvoice')->name('manageInvoice')
    ->middleware('verified');

Route::get('/admin/partners', 'Admin\PartnerManagementController@index')->name('partnerList')
    ->middleware('verified');


Auth::routes([
    'verify' => true
]);

Route::get('/profile', 'ProfilePageController@index')->name('my_profile')
    ->middleware('verified');
Route::get('/profile/{id}', 'ProfilePageController@index')->name('profile')
    ->middleware('verified');
Route::post('/profile/avatar', 'ProfilePageController@changeAvatar')->name('changeAvatar')
    ->middleware('verified');
Route::post('/profile/cover', 'ProfilePageController@changeCover')->name('changeCover')
    ->middleware('verified');


Route::get('/settings', 'SettingsPageController@index')
    ->middleware('auth')
    ->name('settings');
Route::post('/settings', 'SettingsPageController@changeInfo')
    ->middleware('auth')
    ->name('changeInfo');

// Page d'accueil
Route::get('/', 'HomepageController@index')->name('/');
Route::get('/home', 'HomeController@index')->name('home')
    ->middleware('verified');


Route::get('/legalNotice', 'LegalNoticeController@index')->name('legalNotice');
Route::get('/privacy', 'PrivacyController@index')->name('privacy');


// Route de test
// /!\ Commenter avant mise en production /!\
// Route::get('/test', function() {
//     return view('event.unregistered', ['event' => App\Event::find(1)]);
// })->name('view');
