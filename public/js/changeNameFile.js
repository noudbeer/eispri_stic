let changeNameFile = (id_input, id_label) => {
    var fullPath = document.getElementById(id_input).value;
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var fileName = fullPath.substring(startIndex);

        if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0) {
            fileName = fileName.substring(1);
        }
    document.getElementById(id_label).innerHTML = fileName;
    }
}