function showDiv() {
    // Get the checkbox
    var checkBox = document.getElementById("myCheck");
    // Get the output text
    var elem = document.getElementById("elem");
  
    // If the checkbox is checked, display the output text
    if (checkBox.checked == true) {
      elem.style.display = "block";
    } else {
      elem.style.display = "none";
    }
  } 