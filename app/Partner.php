<?php
/*
	Auteur : Julien Valverdé
	Le 18/11/2019
*/
namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'description', 'logo', 'email'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
	];

	/**
	 * Les donnation des entreprises
	 */
	public function donations() {
		return $this->hasMany('App\Donation');
	}

	/**
	 * Récupère la dernière donnation
	 */
	public function lastDonation() {
		return $this->donations()->orderBy('year', 'desc')->first();
	}
}
?>
