<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Event;
use App\User;


class EventUserInWaitingList extends Mailable
{
    use Queueable, SerializesModels;


    protected $event;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Event $event, User $user)
    {
        $this->event = $event;
        $this->user  = $user;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@eispri-stic.fr')
                    ->subject('Vous avez été inscrit à la liste d\'attente de l\'évènement ' . $this->event->name)
                    ->view('mails.event_user_in_waiting_list')
                    ->with(['event' => $this->event, 'user' => $this->user]);
    }
}
?>
