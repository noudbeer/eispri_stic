<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Event;
use App\EventTeam;
use App\User;


class EventUserMovedFromWaitingList extends Mailable
{
    use Queueable, SerializesModels;


    protected $event;
    protected $user;
    protected $team;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Event $event, User $user, EventTeam $team = null)
    {
        $this->event = $event;
        $this->user  = $user;
        $this->team  = $team;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@eispri-stic.fr')
                    ->subject('Vous avez été inscrit à l\'évènement ' . $this->event->name)
                    ->view('mails.event_user_moved_from_waiting_list')
                    ->with(['event' => $this->event, 'user' => $this->user, 'team' => $this->team]);
    }
}
?>
