<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class EventTeam extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id',
        'name',
        'owner_id'
    ];


    /*  --------------------------------------------------
            Évènement
        -------------------------------------------------- */

    /**
     * Retourne l'évènement auquel cette équipe participe
     */
    public function event() {
        return $this->belongsTo('App\Event');
    }


    /*  --------------------------------------------------
            Membres
        -------------------------------------------------- */

    /**
     * Retourne les utilisateurs membres de l'équipe, qu'ils soient inscrits ou sur liste d'attente
     */
    public function users() {
        return $this->event->users()->wherePivot('event_team_id', $this->id);
    }

    /**
     * Retourne les utilisateurs membres de l'équipe triés par leur ordre d'inscription
     */
    public function users_by_registration_order() {
        return $this->users()->orderBy('pivot_updated_at', 'asc');
    }

    /**
     * Retourne le chef de l'équipe
     */
    public function owner() {
        return $this->belongsTo('App\User');
    }

    /**
     * Indique si l'utilisateur donné est membre de l'équipe
     */
    public function isMember(User $user) {
        return $this->users()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Indique si l'utilisateur donné est le propriétaire de l'équipe
     */
    public function isOwner(User $user) {
        return $this->owner->id === $user->id;
    }

}
?>
