<?php

namespace App\Http\Controllers;

use App\Event;
use App\Partner;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use DateTime;

class HomepageController extends Controller
{
    /**
     * Récupère tous les utilisateurs aillant le rôle 'membre'
     */
    private function members() {
        return User::all()->filter(function($user, $key) {
            return $user->hasRole('member');
        });
    }

    public function index()
    {
        //nextEvent
        $now = new DateTime();
        $event = Event::where('end', ">", $now)->orderby('beginning', 'asc')->first();
        $events = Event::where('end', ">", $now)->orderby('beginning', 'asc')->get();

        //all partners sort by donation
        $partners = Partner::all()->sortByDesc('donation');

        $nb_members = $this->members()->count();
        $members = $this->members();

        return view('index', compact(['event', 'partners', 'nb_members', 'members', 'events']));
    }
}
