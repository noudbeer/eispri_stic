<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Study;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Redirect;

class SettingsPageController extends Controller
{
    function index()
    {
        //abort(403, 'Le panneau de l\'utilisateur n\'est pas encore opérationnel. Pour tout problème, veuillez utiliser la page de contact.
        //Toutes nos excuses.');

        $user = Auth::user();
        $studies = Study::all();
        return view('/settings', compact(['user', 'studies']));
    }

    function changeInfo(Request $request) {
        $user = Auth::user();
        $studies = Study::all();

        $validatedData = $request->validate([
            'firstname'		            => ['nullable', 'string', 'max:255'],
            'lastname'		            => ['nullable', 'string', 'max:255'],
            'study'                     => ['nullable', 'integer', 'max:255'],
            'new_password'              => ['nullable', 'string', 'max:255', 'min:6|required_with:new_password_confirmation|same:new_password_confirmation'],
            'new_password_confirmation' => ['nullable', 'string', 'max:255', 'min:6'],
            // 'email'                     => ['nullable', 'string', 'email', 'regex:/^[a-z]+\.[a-z]+@etu\.univ-smb\.fr$/', 'max:255', 'unique:users,email', 'confirmed'],
            'new_email'                 => ['nullable', 'string', 'email', 'max:255', 'unique:users,email', 'confirmed'],
            'password'                  => ['required'],
        ], [
            // 'email.regex'               => 'Veuillez utiliser votre adresse email universitaire (prenom.nom@etu.univ-smb.fr)'
        ]);

        //Check si password correcte
        if(Hash::check($validatedData['password'], $user->password)) {
            $email_changed = false;


            if($validatedData['firstname'] != null)
                $user->firstname = $validatedData['firstname'];
            if($validatedData['lastname'] != null)
                $user->lastname = $validatedData['lastname'];
            if($validatedData['study'] != null)
                $user->study_id = $validatedData['study'];

            // Check si le nouveau mot de passe est bien identique dans les 2 champs
            if($validatedData['new_password'] != null && $validatedData['new_password_confirmation'] != null) {
                if($validatedData['new_password'] == $validatedData['new_password_confirmation'])
                    $user->password = Hash::make($validatedData['new_password']);
            }
            if($validatedData['new_email'] != null) {
                $user->email             = $validatedData['new_email'];
                $user->email_verified_at = null;

                $email_changed = true;
            }


            $user->save();

            if ($email_changed) {
                $user->sendEmailVerificationNotification();  // Envoyer un e-mail de vérification
                return redirect(route('home'));              // Rediriger sur la page de vérification
            }
            else
                return view('/settings', compact(['user', 'studies']));
        }
        else
            return redirect()->back()->with('error', 'Mot de passe incorrecte');
    }
}
