<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Event;
use App\EventTeam;
use App\EventUser;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Mail;
use App\Mail\EventUserRegistered;
use App\Mail\EventUserInWaitingList;
use App\Mail\EventUserUnregistered;
use App\Mail\EventUserMovedFromWaitingList;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nextEvents = Event::getNextEvents();
        $pastEvent = Event::getPastEvents();
        return view("events", compact('nextEvents', 'pastEvent'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        if ($event === NULL)
            abort(404);

        $now = new \DateTime('now');

        return view('event', compact(['event', 'now']));
    }

    /**
     * Permet à un utilisateur de s'inscrire à un évènement
     */
    public function register($event_id) {
        // On récupère l'utilisateur et l'évènement
        $user  = Auth::user();
        $event = Event::find($event_id);

        if ($event === null)
            abort(404);

        switch ($this->registerUser($event, $user)) {
            case 0:  return view('event.registered', compact(['event']));    // Utilisateur inscrit
            case 1:  abort(403);                                             // Inscription impossible pour cet utilisateur
            case 2:  return view('event.waiting_list', compact(['event']));  // Utilisateur mis sur liste d'attente
        }
    }

    /**
     * Inscrit un utilisateur à l'évènement
     */
    private function registerUser(Event $event, User $user, EventTeam $team = null) {
        // On vérifie que l'inscription est possible pour l'utilisateur donné
        if ($event->registrationEnded() || $event->inRelation($user))  // Si les inscriptions sont terminées, ou si l'utilisateur est déjà en relation avec l'évènement
            return 1;

        // Si l'évènement a encore de la place, inscrire l'utilisateur
        if (!$event->full()) {
            $event->addParticipant($user, $team);

            Mail::to($user)
                ->send(new EventUserRegistered($event, $user, $team));

            return 0;
        }
        // Sinon, mettre l'utilisateur sur liste d'attente
        else {
            $event->addWaitingList($user, $team);

            Mail::to($user)
                ->send(new EventUserInWaitingList($event, $user, $team));

            return 2;
        }
    }

    /**
     * Permet à un utilisateur de se désinscrire d'un évènement
     */
    public function unregister($event_id) {
        // On récupère l'utilisateur et l'évènement
        $user  = Auth::user();
        $event = Event::find($event_id);

        if ($event === null)
            abort(404);

        $user = $event->user($user);  // On ajoute le pivot à l'objet utilisateur

        switch ($this->unregisterUser($event, $user)) {
            case 0:          return view('event.unregistered', compact(['event']));
            case 1:          abort(403);
            case 2: case 3:  abort(500);
        }
    }

    /**
     * Désinscrit un utilisateur d'un évènement
     */
    private function unregisterUser(Event $event, User $user) {
        // On vérifie que la désinscription est possible pour l'utilisateur donné
        if ($event->registrationEnded() || !$event->inRelation($user))  // Si les inscriptions sont terminées, ou si l'utilisateur n'est pas déjà en relation avec l'évènement
            return 1;

        // Si l'utilisateur fait partie d'une équipe, on regarde s'il en est le chef
        $owned_team = null;
        if ($user->pivot->event_team_id !== null) {
            $team = EventTeam::find($user->pivot->event_team_id);

            if ($team === null)  // Si l'équipe n'a pas été trouvée
                return 2;

            if ($team->isOwner($user))  // Si l'utilisateur est chef de son équipe, on la garde en mémoire
                $owned_team = $team;
        }

        // Retrait de l'utilisateur de la liste
        $user_on_wl = $event->inWaitingList($user);  // L'utilisateur est-il inscrit ou sur liste d'attente ?
        $event->removeUser($user);

        Mail::to($user)
            ->send(new EventUserUnregistered($event, $user));

        // Si l'utilisateur n'était pas sur liste d'attente, piocher le premier utilisateur inscrit à liste d'attente (premier arrivé, premier servi !)
        if (!$user_on_wl) {
            $first_wl_user = $event->waiting_list()->orderBy('created_at', 'asc')->first();
            if ($first_wl_user !== null)  // Si un tel utilisateur existe, l'inscrire à l'évènement
                $this->moveFromWaitingList($event, $first_wl_user);
        }

        // Si l'utilisateur était chef de son équipe, on en désinscrit tous les membres et on la supprime
        if ($owned_team !== null) {
            foreach ($owned_team->users as $member) {  // On désinscrit chaque membre
                if ($this->unregisterUser($event, $member) !== 0)
                    return 3;
            }

            $owned_team->delete();  // On supprime l'équipe
        }

        return 0;
    }

    /**
     * Déplace un utilisateur de la liste d'attente vers la liste des inscrits
     */
    private function moveFromWaitingList(Event $event, User $user) {
        if (!$event->inWaitingList($user))  // L'utilisateur n'est pas dans la liste d'attente
            abort(500);

        if ($event->full())  // L'évènement est plein
            abort(500);

        // On extrait l'équipe à laquelle l'utilisateur appartient. Si ce n'est pas un évènement par équipe, celle-ci vaudra null
        $team = ($event->team()) ? EventTeam::find($user->event_team_id) : null;

        $event->removeUser($user);
        $event->addParticipant($user, $team);

        Mail::to($user)
            ->send(new EventUserMovedFromWaitingList($event, $user));
    }

    /**
     * Affiche la page de création d'une nouvelle équipe
     */
    public function showNewTeamPage($event_id) {
        // On récupère l'utilisateur et l'évènement
        $user  = Auth::user();
        $event = Event::find($event_id);

        if ($event === null)
            abort(404);

        // On vérifie que l'inscription est possible pour l'utilisateur donné
        if ($event->registrationEnded() || $event->inRelation($user) || !$event->team())  // Si les inscriptions sont terminées, ou si l'utilisateur est déjà en relation avec l'évènement, ou encore si l'évènement ne permet pas l'inscription par équipe
            abort(403);

        return view('event.new_team', compact(['event']));
    }

    /**
     * Permet à un utilisateur de créer une nouvelle équipe pour l'évènement
     */
    public function registerNewTeam(Request $request) {
        // Valider les données envoyées par le formulaire
        $data = $request->validate([
            'event_id' => 'required|exists:App\Event,id',
            'name'     => 'required|string'
        ]);

        // On récupère l'utilisateur et l'évènement
        $user  = Auth::user();
        $event = Event::find($data['event_id']);

        // On vérifie que l'inscription est possible pour l'utilisateur donné
        if ($event->registrationEnded() || $event->inRelation($user) || !$event->team())  // Si les inscriptions sont terminées, ou si l'utilisateur est déjà en relation avec l'évènement, ou encore si l'évènement ne permet pas l'inscription par équipe
            abort(403);

        // Créer l'équipe
        $team = EventTeam::create([
            'event_id' => $event->id,
            'name'     => $data['name'],
            'owner_id' => $user->id
        ]);

        // Inscrire l'utilisateur
        switch ($this->registerUser($event, $user, $team)) {
            case 0:  return view('event.registered', compact(['event', 'team']));    // Utilisateur inscrit
            case 1:  abort(403);                                                     // Inscription impossible pour cet utilisateur
            case 2:  return view('event.waiting_list', compact(['event', 'team']));  // Utilisateur mis sur liste d'attente
        }
    }

    /**
     * Permet à un chef de désinscrire son équipe
     * À ÉVENTUELLEMENT SUPPRIMER
     */
    // public function disbandTeam($team_id) {
    //     // On récupère l'utilisateur, l'équipe et l'évènement associé
    //     $user  = Auth::user();
    //     $team  = EventTeam::find($team_id);
    //     $event = $team->event;

    //     // On vérifie que la désinscription est possible pour l'utilisateur donné
    //     if ($event->registrationEnded() || !$team->isOwner($user))  // Si les inscriptions sont terminées, ou si l'utilisateur n'est pas le chef du groupe
    //         abort(403);

    //     // On désinscrit tous les membres
    //     foreach ($team->users as $member) {

    //     }

    //     // On supprime l'équipe
    //     $team->delete();
    // }

    /**
     * Affiche la page de gestion d'une équipe
     */
    public function showTeamPage($team_id) {
        // On récupère l'utilisateur et l'équipe
        $user = Auth::user();
        $team = EventTeam::find($team_id);

        if ($team === null)
            abort(404);
        if (!$team->isMember($user))  // L'utilisateur doit être membre de l'équipe pour pouvoir accéder à sa page
            abort(403);

        // Tous les utilisateurs de la team
        $membersTeam    = $team->users_by_registration_order;
        $nb_membersTeam = $membersTeam->count();

        // Le nombre maximum d'équipier
        $membresMax = $team->event->team_size;
        $results    = NULL;

        return view('event.team', compact(['team', 'membersTeam', 'nb_membersTeam', 'membresMax']));
    }

    public function searchMate(Request $request, $team_id) {
        // On récupère l'utilisateur et l'équipe
        $user = Auth::user();
        $team = EventTeam::find($team_id);

        // Tous les utilisateurs de la team
        $membersTeam = EventTeam::find($team_id)->users()->get();
        $nb_membersTeam = $membersTeam->count();

        // Le nombre maximum d'équipier
        $id_event = EventTeam::find($team_id)->event_id;
        $membresMax = Event::find($id_event)->team_size;

        $search = $request->all()['searchBar'];
        $search = $this->normalize($search);
        $search = strtoupper($search);

        $results = User::all()->filter(function($user, $key) use($search, $id_event) {
            $fullname = $user->firstname . ' ' . $user->lastname;
            $fullname = $this->normalize($fullname);
            $fullname = strtoupper($fullname);

            $keywords = explode(' ', $search);

            if(Event::find($id_event)->participates($user))
                return false;

            foreach ($keywords as $keyword) {
                if (preg_match('/' . $search . '/', $fullname))
                    return true;
            }
            return false;
        });

        return view('event.team', compact(['team', 'membersTeam', 'nb_membersTeam', 'membresMax', 'results']));
    }

    private function normalize($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );
        return strtr($string, $table);
    }

    public function addMate($team_id, $mate_id) {
        $id_event = EventTeam::find($team_id)->event_id;
        $event = Event::find($id_event);
        $user = User::find($mate_id);

        // On récupère l'utilisateur et l'équipe
        $team = EventTeam::find($team_id);
        // Tous les utilisateurs de la team
        $membersTeam = EventTeam::find($team_id)->users()->get();
        $nb_membersTeam = $membersTeam->count();
        // Le nombre maximum d'équipier
        $membresMax = Event::find($id_event)->team_size;

        if ($nb_membersTeam < $membresMax && !$event->participates($user)) {
            if ($this->registerUser($event, $user, $team) !== 0)
                abort(500);

            return redirect(route('event.show_team', $team_id));
        }
        else
            abort(403);
    }

    public function showTeams($event_id) {
        $event = Event::find($event_id);
        $teams = EventTeam::where('event_id', $event_id)->get();

        return view('event.teamsViewer', compact('event', 'teams'));
    }

    public function showParticipants($event_id) {
        $event = Event::find($event_id);
        $participants = $event->participants('event_id', $event_id)->get();

        return view('event.participantsViewer', compact('event','participants'));
    }
}
