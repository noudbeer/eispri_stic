<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;


class ProfilePageController extends Controller
{
    public function index($id = null)
    {
        $now = new \DateTime('now');


        $user     = ($id !== null) ? User::find($id) : Auth::user();  // Utilisateur à afficher. Si aucun n'a été précisé, afficher l'utilisateur courant
        if ($user === null)  // Utilisateur non trouvé
            abort(404);

        $editable = $user->is(Auth::user());  // Définit si l'utilisateur courant peut éditer le profil (càd. si c'est le sien)


        $nb_registrations_passed = $user->events()->where('end', '<', $now) ->orderby('beginning', 'desc')->count();
        $nb_next_registrations   = $user->events()->where('end', '>=', $now)->orderby('beginning', 'desc')->count();
        $registrations_passed    = $user->events()->where('end', '<', $now) ->orderby('beginning', 'asc') ->get();
        $next_registrations      = $user->events()->where('end', '>=', $now)->orderby('beginning', 'asc') ->get();

        return view('profile', compact('user', 'editable', 'nb_registrations_passed', 'nb_next_registrations', 'registrations_passed', 'next_registrations'));
    }

    /**
     * Changement de photo de profil
     *
     * @return view
     */
    public function changeAvatar(Request $request) {
        $now = new \DateTime('now');


        $user     = Auth::user();
        $editable = true;


        $nb_registrations_passed = $user->events()->where('end', '<', $now)->orderby('beginning', 'desc')->count();
        $nb_next_registrations = $user->events()->where('end', '>=', $now)->orderby('beginning', 'desc')->count();

        $registrations_passed = $user->events()->where('end', '<', $now)->orderby('beginning', 'asc')->get();
        $next_registrations = $user->events()->where('end', '>=', $now)->orderby('beginning', 'asc')->get();

        $validatedData = $request->validate([
            'avatar' => ['file', 'mimetypes:image/jpeg,image/png'],
        ]);

        if ($request->hasFile('avatar'))
            $path = $request->avatar->storeAs('public/img/users/'.$user->id, 'avatar');
        else
            abort(500);

        return view('profile', compact('user', 'editable', 'nb_registrations_passed', 'nb_next_registrations', 'registrations_passed', 'next_registrations'));
    }

    /**
     * Changement de photo de couverture
     *
     * @return view
     */
    public function changeCover(Request $request) {
        $now = new \DateTime('now');


        $user     = Auth::user();
        $editable = true;


        $nb_registrations_passed = $user->events()->where('end', '<', $now)->orderby('beginning', 'desc')->count();
        $nb_next_registrations = $user->events()->where('end', '>=', $now)->orderby('beginning', 'desc')->count();

        $registrations_passed = $user->events()->where('end', '<', $now)->orderby('beginning', 'asc')->get();
        $next_registrations = $user->events()->where('end', '>=', $now)->orderby('beginning', 'asc')->get();

        $validatedData = $request->validate([
            'cover' => ['file', 'mimetypes:image/jpeg,image/png'],
        ]);

        if ($request->hasFile('cover'))
            $path = $request->cover->storeAs('public/img/users/'.$user->id, 'cover');
        else
            abort(500);

        return view('profile', compact('user', 'editable', 'nb_registrations_passed', 'nb_next_registrations', 'registrations_passed', 'next_registrations'));
    }
}
