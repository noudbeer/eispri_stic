<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function checkUserVerified() {
		// Rediriger l'utilisateur s'il n'a pas vérifié son adresse email
		if (Auth::check() && !Auth::user()->isVerified())
			return redirect('email/verify');
	}
}
?>
