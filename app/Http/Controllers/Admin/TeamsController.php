<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Event;
use App\EventTeam;

class TeamsController extends Controller {

    public function selectEvent() {
        $allEvents = Event::orderBy('beginning', 'desc')->get();
        
        $events = $allEvents->filter(function($event, $key) {
            return $event->team();
        });

        return view('adminTeamsSelectEvent', compact('events'));
    }
}