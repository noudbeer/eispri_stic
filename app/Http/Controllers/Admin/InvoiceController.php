<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class InvoiceController extends Controller {

    public function index() {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        return view('invoiceFrom');
    }

    public function previewInvoice(Request $request) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        if(isset($request->all()['preview'])) {
            $validatedData = $request->validate([
                'name'          =>      'string|max:255|required',
                'address'        =>      'string|required',
                'email'         =>      'email|required',
                'subject'       =>      'string|required',
                'price'         =>      'integer|required',
                'deadline'      =>      'date|required'
            ]);

            $validatedData['now'] = date("Y/m/d");

            return view('previewInvoice', $validatedData);
        }

        abort(500);
    }
}
