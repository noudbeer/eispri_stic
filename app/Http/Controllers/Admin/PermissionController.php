<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class PermissionController extends Controller {

    private function admins() {
        return User::all()->filter(function($user, $key) {
            return $user->hasRole('admin');
        });
    }

    private function superadmins() {
        return User::all()->filter(function($user, $key) {
            return $user->hasRole('superadmin');
        });
    }

    private function members() {
        return User::all()->filter(function($user, $key) {
            return $user->hasRole('member');
        });
    }

    public function index() {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $admins = $this->admins();
        $super = $this->superadmins();
        $members = $this->members();

        return view('permission', compact('admins', 'super', 'members'));
    }

    private function normalize ($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );
        return strtr($string, $table);
    }

    public function search(Request $request) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $admins = $this->admins();
        $super = $this->superadmins();
        $members = $this->members();

        $search = $request->all()['searchBar'];
        $search = $this->normalize($search);
        $search = strtoupper($search);

        $results = User::all()->filter(function($user, $key) use($search) {
            $fullname = $user->firstname . ' ' . $user->lastname;
            $fullname = $this->normalize($fullname);
            $fullname = strtoupper($fullname);

            $keywords = explode(' ', $search);

            foreach ($keywords as $keyword)
                if (preg_match('/' . $search . '/', $fullname))
                    return true;

            return false;
        });

        return view('permission', compact('admins', 'super', 'members', 'results'));
    }

    public function addAdmin($id) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $user = User::find($id);
        $user->role_id = 2;
        $user->save();

        return redirect('admin/permission');
    }

    public function addSuperAdmin($id) {
        Auth::user()->authorizeRoles(['superadmin']);

        $user = User::find($id);
        $user->role_id = 1;
        $user->save();

        return redirect('admin/permission');
    }

    public function addMember($id) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $user = User::find($id);
        $user->role_id = 3;
        $user->save();

        return redirect('admin/permission');
    }

    public function deleteRole($id) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);
        
        $user = User::find($id);
        $user->role_id = 4;
        $user->save();

        if(Auth::user()->id != $id)
            return redirect('admin/permission');
        else
            return redirect('/');
    }
}
