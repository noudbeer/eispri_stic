<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class AdminHomeController extends Controller {

    function index() {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        return view('adminHome');
    }

}
