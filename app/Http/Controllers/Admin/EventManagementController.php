<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Event;
use Illuminate\Http\Request;

use DateTime;

class EventManagementController extends Controller {

    public function index() {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $nextEvents = Event::getNextEvents();
        $pastEvent = Event::getPastEvents();
        return view('eventManagement', compact('nextEvents', 'pastEvent'));
    }

    /**
     * Ajout d'un événement
     */
    public function addEventPage($id = null) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        if($id != null) {
            $event = Event::find($id);
            return view('addEventPage', ['event' => $event]);
        }
        else
            return view('addEventPage');
    }

    // ajout d'un événement dans la bdd
    public function addEvent(Request $request) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $validatedData = $request->validate([
            'id'                    => 'exists:events,id|nullable',
            'title'                 => 'string|max:255|required',
            'image'                 => 'file|mimetypes:image/jpeg,image/png',
            'description_excerpt'   => 'string|max:255',
            'description'           => 'string',
            'date-start'            => 'required',
            'time-start'            => 'required',
            'date-end'              => 'required',
            'time-end'              => 'required',
            'inscription-date-end'  => 'required',
            'inscription-time-end'  => 'required',
            'localisation'          => 'string|max:255',
            'price'                 => 'integer|nullable',
            'room'                  => 'integer|nullable',
            'team_size'             => 'integer|nullable',
        ]);

        $event = ($validatedData['id'] !== null) ? Event::find($validatedData['id']) : new Event();

        $event->name = $validatedData['title'];
        $event->description_excerpt = $validatedData['description_excerpt'];
        $event->description = $validatedData['description'];
        $event->beginning = new DateTime($validatedData['date-start'].' '.$validatedData['time-start']);
        $event->end = new DateTime($validatedData['date-end'].' '.$validatedData['time-end']);
        $event->registration_end = new DateTime($validatedData['inscription-date-end'].' '.$validatedData['inscription-time-end']);
        $event->room = $validatedData['room'];
        $event->location = $validatedData['localisation'];

        if($event->price == null) {
            $event->price = 0;
        }
        else {
            $event->price = $validatedData['price'];
        }

        $event->team_size = $validatedData['team_size'];

        $event->save();

        if ($request->hasFile('image'))
            $request->image->storeAs('public/img/events', $event->id);

        return redirect('/admin/eventManagement');
    }

    public function deleteEvent($id) {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);

        $event = Event::find($id)->delete();
        return redirect('/admin/eventManagement');
    }
}
