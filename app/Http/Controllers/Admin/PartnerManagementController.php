<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Partner;
use App\PartnerDonation;

class PartnerManagementController extends Controller {

    public function index() {
        Auth::user()->authorizeRoles(['admin', 'superadmin']);


        $now = date('Y');
        $partners = Partner::all();

        return view('partnerList', ['partners' => $partners]);
    }
}
