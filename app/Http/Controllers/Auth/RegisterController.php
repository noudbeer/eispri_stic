<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Study;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm()
	{
		return view('auth.login', [
			'studies' => Study::all()
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'reg_firstname'		=> ['required', 'string', 'max:255'],
			'reg_lastname'		=> ['required', 'string', 'max:255'],
			'reg_email'			=> ['required', 'string', 'email', /*'regex:/^[a-z]+\.[a-z]+@etu\.univ-smb\.fr$/',*/ 'max:255', 'unique:users,email', 'confirmed'],
			'reg_password'		=> ['required', 'string', 'min:8', 'confirmed']
		], [
			'reg_email.regex'	=> 'Veuillez utiliser votre adresse email universitaire (prenom.nom@etu.univ-smb.fr)'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{
		return User::create([
			'firstname'		=> $data['reg_firstname'],
			'lastname'		=> $data['reg_lastname'],
			'password'		=> Hash::make($data['reg_password']),
			'study_id'		=> $data['reg_study'],
			'email'			=> $data['reg_email']
		]);
	}
}
?>
