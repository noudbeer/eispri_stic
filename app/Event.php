<?php
/*
    Auteur : Julien Valverdé
    Le 18/11/2019
*/
namespace App;

use Illuminate\Database\Eloquent\Model;
use \DateTime;


class Event extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'description',
        'description_excerpt',
        'location',
        'beginning',
        'end',
        'registration_end',
        'price',
        'room',
        'team_size'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'beginning'         => 'datetime',
        'end'               => 'datetime',
        'registration_end'  => 'datetime'
    ];


    /**
     * getNextEvents
     * Retourne les prochains évènements
     */
    public static function getNextEvents() {
        return static::orderBy('end', 'asc')->get()->filter(function($event, $key) {
            return $event->end > new DateTime();
        });
    }

    /**
     * Retourne les évènements passés
     */
    public static function getPastEvents() {
        return static::orderBy('end', 'desc')->get()->filter(function($event, $key) {
            return $event->end < new DateTime();
        });
    }


    /*  --------------------------------------------------
            Participants et liste d'attente
        -------------------------------------------------- */

    /**
     * Retourne les utilisateurs en relation avec l'évènement, qu'ils soient inscrits ou sur liste d'attente
     */
    public function users() {
        return $this->belongsToMany('App\User')
            ->withPivot(['waiting_list', 'event_team_id', 'created_at', 'updated_at'])
            ->withTimestamps();
    }

    /**
     * Retourne les utilisateurs participants à l'évènement
     */
    public function participants() {
        return $this->users()->wherePivot('waiting_list', false);
    }

    /**
     * Retourne le nombre de participants à l'évènement 
     */
    public function nbParticipants() {
        return count($this->participants);
    }

    /**
     * Retourne les utilisateurs en liste d'attente
     */
    public function waiting_list() {
        return $this->users()->wherePivot('waiting_list', true);
    }

    /**
     * Retourne les équipes participants à cet évènement
     */
    public function teams() {
        return $this->hasMany('App\EventTeam');
    }

    /**
     * Retourne les nombre d'équipes complètes participants à cet évènement
     */
    public function nbFullTeams() {
        $teams = $this->teams();
        $nbTeams = 0;
        foreach ($teams as $team) {
            $nbUsers = 0;
            $users = $team->users();
            foreach($users as $user){
                $nbUsers++;
            }
            return $nbUsers;
            if( $nbUsers >= $this->team_size){
                $nbTeams++;
            }
        }
        return $nbTeams;
    }

    public function nbTeams() {
        $teams = $this->teams();
        $nbTeams = 0;
        foreach ($teams as $team) {
            $nbTeams++;
        }
        return $nbTeams;
    }

    /**
     * Retourne l'objet d'un utilisateur agrémenté de son pivot si cet utilisateur est en relation avec cet évènement, sinon null
     */
    public function user(User $user) {
        return $this->users()->where('user_id', $user->id)->first();
    }

    /**
     * Indique si un utilisateur est en relation avec l'évènement, qu'il soit inscrit ou sur liste d'attente
     */
    public function inRelation(User $user) {
        return $this->users()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Indique si un utilisateur est inscrit à l'évènement
     */
    public function participates(User $user) {
        return $this->participants()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Indique si un utilisateur est en liste d'attente
     */
    public function inWaitingList(User $user) {
        return $this->waiting_list()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Ajoute un utilisateur à la liste des participants
     */
    public function addParticipant(User $user, EventTeam $team = null) {
        $team_id = ($team !== null) ? $team->id : null;

        if (!$this->inRelation($user))
            $this->users()->attach($user->id, [
                'waiting_list'  => false,
                'event_team_id' => $team_id
            ]);
    }

    /**
     * Ajoute un utilisateur à la liste d'attente
     */
    public function addWaitingList(User $user, EventTeam $team = null) {
        $team_id = ($team !== null) ? $team->id : null;

        if (!$this->inRelation($user))
            $this->users()->attach($user->id, [
                'waiting_list'  => true,
                'event_team_id' => $team_id
            ]);
    }

    /**
     * Retire la relation d'un utilisateur avec l'évènement
     */
    public function removeUser(User $user) {
        if ($this->inRelation($user))
            $this->users()->detach($user->id);
    }


    /*  --------------------------------------------------
            Inscriptions
        -------------------------------------------------- */

    /**
     * Retourne l'URL de la page d'inscription à l'évènement
     */
    public function registrationRoute() {
        return (!$this->team()) ? route('event.register', $this->id) : route('event.show_new_team', $this->id);
    }

    /**
     * Retourne l'URL de la page de désinscription à l'évènement
     */
    public function unregistrationRoute() {
        return route('event.unregister', $this->id);
    }

    /**
     * Retourne l'URL de la page de l'équipe à laquelle l'utilisateur donné appartient
     */
    public function teamRoute(User $user) {
        return route('event.show_team',
            $this->user($user)->pivot->event_team_id  // On récupère le pivot de l'utilisateur avec l'évènement pour obtenir le numéro de son équipe
        );
    }

    /**
     * Retourne la date de fin des inscriptions
     */
    public function registrationEnd() {
        return $this->registration_end !== null ? $this->registration_end : $this->beginning;
    }

    /**
     * Indique si la date de fin des inscriptions est passée
     */
    public function registrationEnded() {
        return new DateTime() >= $this->registrationEnd();
    }

    /**
     * Indique si l'évènement est complet
     */
    public function full() {
        return $this->room !== null && count($this->participants) >= $this->room;
    }

    /**
     * Indique si l'évènement requiert une inscription par équipe
     */
    public function team() {
        return $this->team_size !== null;
    }
}
?>
