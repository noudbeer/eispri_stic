<?php
/*
	Auteur : Julien Valverdé
	Le 18/11/2019
*/
namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'partner_id', 'year', 'amount'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
	];
}
?>
