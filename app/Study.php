<?php
/*
	Auteur : Julien Valverdé
	Le 19/11/2019
*/
namespace App;
use Illuminate\Database\Eloquent\Model;


/*
	Study
	Une filière d'étude
*/
class Study extends Model {
	public $timestamps = false;
}
?>
