<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'firstname', 'lastname', 'email', 'password', 'study_id', 'newsletter', 'role'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token'
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime', 'newsletter' => 'bool'
	];

	/*
	* La filière de l'utilisateur
	*/
	public function study() {
		return $this->belongsTo('App\Study');
	}

	/**
	 * Le role de l'utilisateur
	 */
	public function role() {
		return $this->belongsTo('App\Role');
	}

	/*
	 * Indique si l'utilisateur possède un avatar
	 */
	public function getAvatarPath() {
		$path = 'storage/img/users/' . $this->id . '/avatar';

		return file_exists(public_path($path)) ? $path : 'storage/img/default_avatar.png';
	}

	/*
	 * Indique si l'utilisateur possède une photo de couverture
	 */
	public function getCoverPath() {
		$path = 'storage/img/users/' . $this->id . '/cover';

		return file_exists(public_path($path)) ? $path : 'storage/img/default_cover.png';
	}

	/*
	 * Indique si l'utilisateur a vérifié son adresse email
	 */
	public function isVerified() {
		return ($this->email_verified_at != NULL);
	}

	/**
	 * Indique si l'email est une email de l'usmb
	 * @return boolean
	 */
	public function emailUSMB() {
		return preg_match('/^[a-z]+\.[a-z]+@etu\.univ-(smb|savoie)\.fr$/',$this->email); //A tester
	}

	/**
	 * Les évènements auxquels l'utilisateur participe
	 */
	public function events() {
		return $this->belongsToMany('App\Event');
	}

	/**
	 * Check one role
	 *
	 * @param string $role
	 */
	public function hasRole($name) {
		return ($this->role->name === $name);
	}

	/**
	 *
	 * @param string|array $roles
	 *
	 */
	public function authorizeRoles($names) {
		foreach ($names as $name)
			if ($this->hasRole($name))
				return;

		abort(401, 'This action is unauthorized');
	}
}
?>
