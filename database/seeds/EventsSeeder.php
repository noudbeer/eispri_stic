<?php
use Illuminate\Database\Seeder;
use App\Event;

class EventsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $NDI = new Event();
        $NDI->name = 'Nuit de l\'Info 2019';
        $NDI->description = "Le plus fun serious-game regroupant des milliers d’étudiants. Formez des groupes et passez la nuit à développer une application informatique et à relever des défis !
            Inscrivez-vous et choisissez jusqu'à deux amis avec lesquels vous souhaitez rester durant la nuit. Des groupes de six personnes (peut être amené à changé en fonction du nombre d'inscrits) seront ainsi formés.";
        $NDI->description_excerpt = "Le plus fun serious-game regroupant des milliers d’étudiants. Formez des groupes et passez la nuit à développer une application informatique et à relever des défis !";
        $NDI->location = 'USMB Bourget-du-Lac, Bâtiment 12A Maurienne';
        $NDI->beginning = '2019-12-05 16:39:00';
        $NDI->end = '2019-12-06 08:03:00';
        $NDI->registration_end = '2019-11-28 23:59:59';
        $NDI->price = 0.0;
        $NDI->room = null;
        $NDI->save();

        // $inte = new Event();
        // $inte->name = 'Intégration 2020/2021';
        // $inte->description = 'Tu veux veux rencontrer du monde ou boire un verre avec les potes de ta promo ? Rendez-vous au Meltdown mercredi 21 octobre 2020 à partir de 19h30. \r\n\r\nEISPRI STIC vous accueillera avec le Meltdown pour vous faire découvrir ses meilleurs de leurs bières et cocktails.\r\nPromos de la soirée :\r\n - Pinte de Barbar ou Slash Red : 5,50€ au bar\r\n - Shooter de la soirée : 2€ à la table d\'EISPRI STIC';
        // $inte->description_excerpt = 'PLACES LIMITÉES !!';
        // $inte->location = 'Meltdown Chambéry';
        // $inte->beginning = '2020-10-21 19:30:00';
        // $inte->end = '2020-10-22 01:00:00';
        // $inte->price = 0.0;
        // $inte->room = 40;
        // $inte->registration_end = '2020-10-22 00:00:00';
        // $inte->save();
    }
}
