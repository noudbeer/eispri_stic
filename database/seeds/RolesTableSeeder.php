<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_super = new Role();
        $role_super->name = 'superadmin';
        $role_super->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->save();

        $role_member = new Role();
        $role_member->name = 'member';
        $role_member->save();
        
        $role_admin = new Role();
        $role_admin->name = 'user';
        $role_admin->save();
    }
}
