<?php

use Illuminate\Database\Seeder;
use App\Study;

class StudySeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run() {

        $empty = new Study();
        $empty->name = 'Filière non précisée';
        $empty->save();

        $L1 = new Study();
        $L1->name = 'L1 MIST Bourget';
        $L1->save();

        $L2 = new Study();
        $L2->name = 'L2 Informatique Bourget';
        $L2->save();

        $L3 = new Study();
        $L3->name = 'L3 Informatique Bourget';
        $L3->save();

		$M1 = new Study();
        $M1->name = 'M1 Informatique Bourget';
        $M1->save();

        $M2 = new Study();
        $M2->name = 'M2 Informatique Bourget';
        $M2->save();

        $DUT_info = new Study();
        $DUT_info->name = 'DUT MMI Bourget';
        $DUT_info->save();

        $DUT_info = new Study();
        $DUT_info->name = 'DUT Informatique Annecy';
        $DUT_info->save();

        $DUT_MMI = new Study();
        $DUT_MMI->name = 'DUT MMI Bourget';
        $DUT_MMI->save();

        $Polytech_Annecy = new Study();
        $Polytech_Annecy->name = 'Polytech Annecy';
        $Polytech_Annecy->save();

        $other = new Study();
        $other->name = 'Polytech Annecy';
        $other->save();
    }
}