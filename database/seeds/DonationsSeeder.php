<?php

use Illuminate\Database\Seeder;
use App\Donation;

class DonationsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Infomaniak = new Donation();
        $Infomaniak->partner_id = 1;
        $Infomaniak->year = 2019;
        $Infomaniak->amount = 1000.0;
        $Infomaniak->save();

        $Sopra = new Donation();
        $Sopra->partner_id = 2;
        $Sopra->year = 2019;
        $Sopra->amount = 1000.0;
        $Sopra->save();

        $camptocamp = new Donation();
        $camptocamp->partner_id = 3;
        $camptocamp->year = 2019;
        $camptocamp->amount = 500.0;
        $camptocamp->save();
    
        $Capgemini = new Donation();
        $Capgemini->partner_id = 4;
        $Capgemini->year = 2019;
        $Capgemini->amount = 500.0;
        $Capgemini->save();

        $Serial = new Donation();
        $Serial->partner_id = 5;
        $Serial->year = 2019;
        $Serial->amount = 500.0;
        $Serial->save();

        $Kaizen = new Donation();
        $Kaizen->partner_id = 6;
        $Kaizen->year = 2019;
        $Kaizen->amount = 400.0;
        $Kaizen->save();

        $Ippon = new Donation();
        $Ippon->partner_id = 7;
        $Ippon->year = 2019;
        $Ippon->amount = 100.0;
        $Ippon->save();

        $Ark = new Donation();
        $Ark->partner_id = 8;
        $Ark->year = 2019;
        $Ark->amount = 80.0;
        $Ark->save();
    }
}