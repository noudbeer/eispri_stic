<?php
use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Simon = new User();
        $Simon->firstname = 'Simon';
        $Simon->lastname = 'Bernoud';
        $Simon->email = 'simon.bernoud@etu.univ-smb.fr';
        $Simon->password = Hash::make('test');
        $Simon->study_id = 4;
        $Simon->role_id = 1;
	    $Simon->email_verified_at = new \DateTime;
        $Simon->save();
        
        $Julien = new User();
        $Julien->firstname = 'Julien';
        $Julien->lastname = 'Valverdé';
        $Julien->email = 'julien.valverde@etu.univ-smb.fr';
        $Julien->password = Hash::make('test');
        $Julien->study_id = 4;
        $Julien->role_id = 1;
        $Julien->email_verified_at = new \DateTime;
        $Julien->save();
        //
        // $Laurent = new User();
        // $Laurent->firstname = 'Laurent';
        // $Laurent->lastname = 'Cutting';
        // $Laurent->email = 'laurent.cutting@etu.univ-smb.fr';
        // $Laurent->password = Hash::make('test');
        // $Laurent->study_id = 5;
        // $Laurent->role_id = 1;
        // $Laurent->email_verified_at = new \DateTime;
        // $Laurent->save();
        //
        // $Felix = new User();
        // $Felix->firstname = 'Félix';
        // $Felix->lastname = 'Barthe';
        // $Felix->email = 'felix.barthe@etu.univ-smb.fr';
        // $Felix->password = Hash::make('test');
        // $Felix->study_id = 4;
        // $Felix->role_id = 1;
        // $Felix->email_verified_at = new \DateTime;
        // $Felix->save();
        //
        // $etuRand = new User();
        // $etuRand->firstname = 'Etudiant';
        // $etuRand->lastname = 'Random';
        // $etuRand->email = 'test@test.fr';
        // $etuRand->password = Hash::make('test');
        // $etuRand->email_verified_at = new \DateTime;
        // $etuRand->save();
    }
}
