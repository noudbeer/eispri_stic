<?php

use Illuminate\Database\Seeder;
use App\Partner;

class PartnersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $infomaniak = new Partner();
        $infomaniak->name = 'Infomaniak';
        $infomaniak->description = '';
        $infomaniak->logo = 'infomaniak.png';
        $infomaniak->email = '';
        $infomaniak->save();

        $sopra = new Partner();
        $sopra->name = 'Sopra Steria';
        $sopra->description = '';
        $sopra->logo = 'sopra_steria.png';
        $sopra->email = '';
        $sopra->save();

        $camptocamp = new Partner();
        $camptocamp->name = 'Camptocamp';
        $camptocamp->description = '';
        $camptocamp->logo = 'camptocamp.png';
        $camptocamp->email = '';
        $camptocamp->save();

        $Capgemini = new Partner();
        $Capgemini->name = 'Capgemini';
        $Capgemini->description = '';
        $Capgemini->logo = 'capgemini.png';
        $Capgemini->email = '';
        $Capgemini->save();

        $Serial = new Partner();
        $Serial->name = 'Serial';
        $Serial->description = '';
        $Serial->logo = 'serial.png';
        $Serial->email = '';
        $Serial->save();

        $Kaizen = new Partner();
        $Kaizen->name = 'Kaizen';
        $Kaizen->description = '';
        $Kaizen->logo = 'kaizen.png';
        $Kaizen->email = '';
        $Kaizen->save();

        $Ippon = new Partner();
        $Ippon->name = 'Ippon Technologies';
        $Ippon->description = '';
        $Ippon->logo = 'ippon_technologies.png';
        $Ippon->email = '';
        $Ippon->save();

        $Ark = new Partner();
        $Ark->name = 'Ark Innovation';
        $Ark->description = '';
        $Ark->logo = 'ark_innovation.png';
        $Ark->email = '';
        $Ark->save();
    }
}