<?php
/*
	Auteur : Julien Valverdé
	Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		/*
			Structure
		*/
		Schema::create('donations', function(Blueprint $table) {
			$table->bigIncrements('id'); 			// ID

			$table->unsignedBigInteger('partner_id'); 	// ID du partenaire
			$table->foreign('partner_id')
				->references('id')->on('partners');

			$table->integer('year'); 				// Année
			$table->float('amount'); 				// Montant

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('donations');
	}
}
?>
