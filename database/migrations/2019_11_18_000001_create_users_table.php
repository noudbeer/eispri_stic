<?php
/*
    Auteur : Julien Valverdé
    Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function(Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('password');

            // Filière
            $table->unsignedBigInteger('study_id')
                ->nullable();
            $table->foreign('study_id')
                ->references('id')->on('studies')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->string('email')->unique();
            $table->timestamp('email_verified_at') 	//date de vérification de l'email
                ->nullable();

            $table->boolean('newsletter') 			//inscrit à la newsletter ?
                ->default(true);

            $table->unsignedBigInteger('role_id')
                ->default(4);
            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onUpdate('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
?>
