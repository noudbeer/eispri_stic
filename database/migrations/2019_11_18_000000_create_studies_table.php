<?php
/*
	Auteur : Julien Valverdé
	Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudiesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		/*
			Structure
		*/
		Schema::create('studies', function(Blueprint $table) {
			$table->bigIncrements('id'); 	// ID
			$table->string('name'); 		// Nom
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('studies');
	}
}
?>
