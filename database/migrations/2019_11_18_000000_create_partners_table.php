<?php
/*
	Auteur : Julien Valverdé
	Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		/*
			Structure
		*/
		Schema::create('partners', function(Blueprint $table) {
			$table->bigIncrements('id'); 	// ID

			$table->string('name'); 		// Nom
			$table->text('description'); 	// Description
			$table->string('logo'); 		// Logo
			$table->string('email'); 		// Email

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('partners');
	}
}
?>
