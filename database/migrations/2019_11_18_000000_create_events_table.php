<?php
/*
    Auteur : Julien Valverdé
    Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('events', function(Blueprint $table) {
            $table->id();

            $table->string('name');                   // Nom
            $table->text  ('description');            // Description
            $table->text  ('description_excerpt');    // Extrait de la description
            $table->text  ('location');               // Lieu de l'évènement

            $table->dateTime('beginning');            // Date et heure de début
            $table->dateTime('end');                  // Date et heure de fin
            $table->dateTime('registration_end')      // Date et heure de clôture des inscriptions
                ->nullable();                         // null => clôture des inscriptions au début de l'évènement

            $table->float  ('price')                  // Prix d'entrée
                ->nullable();                         // null => évènement gratuit
            $table->integer('room')                   // Nombre de places
                ->nullable();                         // null => nombre illimité de places
            $table->integer('team_size')              // Taille des équipes
                ->nullable();                         // null => inscriptions individuelles

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('events');
    }
}
?>
