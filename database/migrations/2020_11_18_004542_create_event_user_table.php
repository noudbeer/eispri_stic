<?php
/*
    Auteur : Julien Valverdé
    Le 18/11/2019
*/
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventUserTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('event_user', function(Blueprint $table) {
            $table->id();

            // Évènement
            $table->unsignedBigInteger('event_id');
            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // Utilisateur
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // Liste d'attente
            $table->boolean('waiting_list')
                ->default(false);

            // Équipe (si l'évènement requiert une inscription par équipe)
            $table->unsignedBigInteger('event_team_id')
                ->nullable();
            $table->foreign('event_team_id')
                ->references('id')->on('event_teams')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('event_user');
    }
}
?>
